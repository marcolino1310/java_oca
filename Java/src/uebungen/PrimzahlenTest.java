package uebungen;

public class PrimzahlenTest {

	public static void main(String[] args) {
//		Schreiben Sie ein Programm, das alle Zahlen von 2 bis 1000 durchläuft, bei jedem Wert prüft, 
//		ob es sich um eine Primzahl handelt und diese gegebenenfalls auf der Konsole ausgibt. 
		boolean istPrimzahl; // flag
		for (int zahl = 2; zahl <= 1_000_000; zahl++) {
			istPrimzahl = true; // Flag auf Ausgansgwert setzen
			// zahl = 997
			for (int i = 2; i <= Math.sqrt(zahl); i++) {
				if (zahl % i == 0) {
					// System.out.println(zahl + " ist keine Primzahl.");
					istPrimzahl = false; // Flag negieren, keine Primzahl
					break;
				}
			}
			if (istPrimzahl) { // Kurzform von if (istPrimzahl == true)
				System.out.println(zahl + " ist eine Primzahl.");
			}
		}
	}

}
