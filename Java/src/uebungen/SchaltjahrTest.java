package uebungen;

import java.util.Scanner;

public class SchaltjahrTest {

	public static void main(String[] args) {
//		Ein Jahr ist ein Schaltjahr, wenn es durch 4 teilbar ist.
//		Ausnahme: Jahrhundertwenden sind nur ein Schaltjahr, wenn sie durch 400 teilbar sind
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Jahreszahl ein.");
		int jahr = sc.nextInt();

		if (jahr % 4 == 0) {
			if (jahr % 400 == 0) {
				System.out.println(jahr + " ist ein Schaltjahr.");
			} else {
				if (jahr % 100 == 0) {
					System.out.println(jahr + " ist kein Schaltjahr.");
				} else {
					System.out.println(jahr + " ist ein Schaltjahr.");
				}
			}
		} else {
			System.out.println(jahr + " ist kein Schaltjahr.");
		}
		
		if (jahr % 4 == 0 && jahr % 100 != 0 || jahr % 400 == 0) {
			System.out.println(jahr + " ist ein Schaltjahr.");
		} else {
			System.out.println(jahr + " ist kein Schaltjahr.");
		}

	}

}
