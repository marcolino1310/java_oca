package uebungen;

import java.text.NumberFormat;
import java.util.Scanner;

public class Sparzinsrechner {

	public static void main(String[] args) {
//		Programmieren Sie einen Sparzinsrechner, der folgendes leistet: der Anwender kann �ber 3 Eingaben das Startkapital, 
//		die Laufzeit in Jahren sowie den Zinssatz eingeben. Ausgegeben werden soll das Endkapital am Ende der Laufzeit.
		
		// Eingabe der drei Werte
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie das Startkapital ein.");
		int startkapital = sc.nextInt();
		System.out.println("Bitte geben Sie den Zinssatz in Prozent ein.");
		double zinssatz = sc.nextDouble();
		System.out.println("Bitte geben Sie die Laufzeit in Jahren ein.");
		int laufzeit = sc.nextInt();
		
		// Verarbeitung: Berechnung des Endkapitals
		double endkapital = ((startkapital * zinssatz * laufzeit) / 100) + startkapital; // Zinsformel
		endkapital = startkapital * Math.pow(1 + zinssatz / 100, laufzeit); // Zinseszinsformel
		
		// Ausgabe des Endkapital
		// Test 1000 - 5 - 5 => 1276,28
		System.out.println("Ihr Endkapital beträgt: " + endkapital);
		System.out.printf("Ihr Endkapital beträgt: %,.2f €%n",endkapital);
		System.out.println("Ihr Endkapital beträgt: " + NumberFormat.getCurrencyInstance().format(endkapital));
		
		// Ausgabe der Kapitalentwicklung
		// 1    1050,00
		// 2    1110,56
		// ...
		// 10   1628,65
		
		for(int jahr = 1; jahr <= laufzeit; jahr++) {
//			System.out.println(jahr + " " + startkapital * Math.pow(1 + zinssatz / 100, jahr));
			System.out.printf("%3d %,.2f €%n", jahr, startkapital * Math.pow(1 + zinssatz / 100, jahr));

		}
		
		

		sc.close();
	}

}
