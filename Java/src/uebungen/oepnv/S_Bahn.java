package uebungen.oepnv;

public class S_Bahn extends Verkehrsmittel{
	
	private String schaffner;
	private int anzahlWagen;
	private int anzahlSitzplaetzeProWagen;
	
	public S_Bahn() {}

	public S_Bahn(int liniennummer, String fahrer, String start, String ziel, String schaffner, int anzahlWagen,
			int anzahlSitzplaetzeProWagen) {
		super(liniennummer, fahrer, start, ziel);
		this.setSchaffner(schaffner);
		this.setAnzahlWagen(anzahlWagen);
		this.setAnzahlSitzplaetzeProWagen(anzahlSitzplaetzeProWagen);
	}

	public String getSchaffner() {
		return schaffner;
	}

	public void setSchaffner(String schaffner) {
		if (schaffner == null || schaffner.length() < 2) {
			throw new IllegalArgumentException("Schaffner darf nicht null sein und muss mind. 2 Zeichen enthalten: " + schaffner);
		}
		this.schaffner = schaffner;
	}

	public int getAnzahlWagen() {
		return anzahlWagen;
	}

	public void setAnzahlWagen(int anzahlWagen) {
		if (anzahlWagen < 1 || anzahlWagen > 8) {
			throw new IllegalArgumentException("Anzahl der Wagen muss zwischen 1 und 8 liegen: " + anzahlWagen);
		}
		this.anzahlWagen = anzahlWagen;
	}

	public int getAnzahlSitzplaetzeProWagen() {
		return anzahlSitzplaetzeProWagen;
	}

	public void setAnzahlSitzplaetzeProWagen(int anzahlSitzplaetzeProWagen) {
		if (anzahlSitzplaetzeProWagen < 120 || anzahlSitzplaetzeProWagen > 200) {
			throw new IllegalArgumentException("Anzahl der Sitzplätze pro Wagen muss zwischen 120 und 200 liegen: " + anzahlSitzplaetzeProWagen);
		}
		this.anzahlSitzplaetzeProWagen = anzahlSitzplaetzeProWagen;
	}

	@Override
	public String toString() {
		return super.toString() + ", Schaffner: " + schaffner + ", Anzahl Wagen: " + anzahlWagen + ", Sitzplätze pro Wagen: " + anzahlSitzplaetzeProWagen;
	}
	
	public void fortbewegen() {
		System.out.println("Ich fahre mit Strom aus der Oberleitung.");
	}
	
	
	

}
