package uebungen.oepnv;

public abstract class Verkehrsmittel {
	
	private int liniennummer;
	private String fahrer;
	private String start;
	private String ziel;
	
	public Verkehrsmittel() {}

	public Verkehrsmittel(int liniennummer, String fahrer, String start, String ziel) {
		this.setLiniennummer(liniennummer);
		this.setFahrer(fahrer);
		this.setStart(start);
		this.setZiel(ziel);
	}

	public int getLiniennummer() {
		return liniennummer;
	}

	public void setLiniennummer(int liniennummer) {
		if (liniennummer <= 0) {
			throw new IllegalArgumentException("Linie muss positiv sein: " + liniennummer);
		}
		this.liniennummer = liniennummer;
	}

	public String getFahrer() {
		return fahrer;
	}

	public void setFahrer(String fahrer) {
		if (fahrer == null || fahrer.length() < 2) {
			throw new IllegalArgumentException("Fahrer darf nicht null sein und muss mind. 2 Zeichen enthalten: " + fahrer);
		}
		this.fahrer = fahrer;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		if (start == null || start.length() < 2) {
			throw new IllegalArgumentException("Starthaltestelle darf nicht null sein und muss mind. 2 Zeichen enthalten: " + start);

		}
		this.start = start;
	}

	public String getZiel() {
		return ziel;
	}

	public void setZiel(String ziel) {
		if (ziel == null || ziel.length() < 2) {
			throw new IllegalArgumentException("Zielhaltestelle darf nicht null sein und muss mind. 2 Zeichen enthalten: " + ziel);

		}
		this.ziel = ziel;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName().replace("_", "-") + " Linie " + liniennummer + ", Fahrer: " + fahrer + ", Start: " + start + ", Ziel: "
				+ ziel;
	}
	
//	Zwei Verkehrsmittel sind von der Bedeutung her (semantisch) gleich, wenn sie die gleiche Liniennummer haben
	public boolean equals(Verkehrsmittel v) {
		return this.liniennummer == v.liniennummer;
	}
	
//	Eine abstrakte Methode hat keine Implementierung. 
//	Enthält eine Klasse eine abstrakte Methode, so muss sie als abstract definiert werden.
//	Eine abstrakte Klasse muss keine abstrakten Methoden definieren.
//	Alle (nicht abstrakten) Unterklassen müssen die abstrakte Methode überschreiben/implementieren
	public abstract void fortbewegen();

}
