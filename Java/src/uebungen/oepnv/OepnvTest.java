package uebungen.oepnv;

public class OepnvTest {

	public static void main(String[] args) {
//		Modellieren Sie den ÖPNV. Ein öffentliches Verkehrsmittel besitzt eine Liniennummer, einen Fahrer, sowie eine Start- und eine 
//		Zielhaltestelle, ein Bus besitzt zusätzlich die Anzahl der Sitzplätze, eine S-Bahn hat – zusätzlich zu ihren Eigenschaften als 
//		Verkehrsmittel -  den Namen des Schaffners, die Anzahl der Wagen und die Anzahl der Sitzplätze pro Wagen. Die Namen der Fahrer, 
//		Schaffner und Haltestellen werden als Zeichenketten angegeben. Die Liniennumer, die Anzahl der Wagen und die Sitzplätze werden als 
//		ganze Zahlen gespeichert. Die Verkehrsmittel sollen eine vernünftige Textdarstellung bekommen. 
//		 
//		Programmieren Sie die Klassen Verkehrsmittel, Bus und S-Bahn, wobei Sie vom Konzept der Vererbung Gebrauch machen sollen. 
//		 
//		Erstellen Sie für jede Klasse den Standardkonstruktor sowie den Konstruktor, der alle Attribute initialisiert. 
//		 
//		Definieren Sie für die set-Methoden sinnvolle Gültigkeitsüberprüfungen. 
//		 
//		Überschreiben Sie die toString-Methode für eine formatierte Ausgabe aller Attributwerte. 
//		 
//		Definieren Sie die equals-Methode, so dass zwei Verkehrsmittel gleich sind bei gleicher Liniennummer
//		Verkehrsmittel v1 = new Verkehrsmittel(); // Compiler-Fehler, da Verkehrsmittel abstrakt ist
//		System.out.println(v1.toString());
		Verkehrsmittel v2 = new S_Bahn(1, "Schmidt", "Dortmund Hbf", "Solingen Hbf", "Abel", 2, 200);
		System.out.println(v2.toString());
		Verkehrsmittel v3 = new Bus(933, "Meier", "Waldfriedhof", "Hauptbahnhof", 120);
		System.out.println(v3);
		Verkehrsmittel v4 = new S_Bahn(1, "Schmidt", "Dortmund Hbf", "Solingen Hbf", "Abel", 2, 200);
		System.out.println(v4);
		Verkehrsmittel v5 = v2; // v5 referenziert auf DASSELBE Objekt wie v2
//		Sind v2 und v4 die selben oder die gleichen Objekte?
		boolean istGleich = v2.equals(v3);
		System.out.println("istGleich = " + istGleich);
		istGleich = v2.equals(v4);
		System.out.println("istGleich = " + istGleich);
		istGleich = v2.equals(v5);
		System.out.println("istGleich = " + istGleich);
		info(v3);
	}
	
	public static void info(Verkehrsmittel v) {
		System.out.println("Info über das Verkehrsmittel");
		System.out.println(v);
		v.fortbewegen();
	}

}
