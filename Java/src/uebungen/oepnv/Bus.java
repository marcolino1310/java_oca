package uebungen.oepnv;

public class Bus extends Verkehrsmittel{
	
	private int anzahlSitzplaetze;
	
	public Bus() {}
	
	public Bus(int liniennummer, String fahrer, String start, String ziel, int anzahlSitzplaetze) {
		super(liniennummer, fahrer, start, ziel);
		this.setAnzahlSitzplaetze(anzahlSitzplaetze);
	}

	public int getAnzahlSitzplaetze() {
		return anzahlSitzplaetze;
	}

	public void setAnzahlSitzplaetze(int anzahlSitzplaetze) {
		if (anzahlSitzplaetze < 24 || anzahlSitzplaetze > 144) {
			throw new IllegalArgumentException("Anzahl der Sitzplätze muss zwischen 24 und 144 liegen: " + anzahlSitzplaetze);
		}
		this.anzahlSitzplaetze = anzahlSitzplaetze;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Anzahl Sitzplätze: " + anzahlSitzplaetze;
	}
	
	public void fortbewegen() {
		System.out.println("Ich fahre mit Diesel.");
	}
}
