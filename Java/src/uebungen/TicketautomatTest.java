package uebungen;

import java.util.Scanner;

public class TicketautomatTest {

	public static void main(String[] args) {
//		Schreiben Sie ein Programm, das einen Fahrscheinautomaten des VRR  simuliert.  
//		• Zunächst soll der Benutzer aus einem Menü mit den zur Verfügung stehenden Tarifzonen (K, A, B, C, D) auswählen können.  
//		• Anschließend kann der Benutzer solange Geld einwerfen, bis der Fahrpreis bezahlt ist. 
//		• Der Automat darf nur gültige Scheine (bis 20 €) und Münzen annehmen.  
//		• Wird zu viel Geld eingeworfen, gibt der Automat Wechselgeld zurück.  
//		• Geben Sie auf dem Bildschirm aus, welche Münzen als Wechselgeld ausgegeben werden.  
//		• Anschließend soll der Automat für den nächsten Benutzer wieder einsatzbereit sein. 

		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte wählen Sie die Tarifzone (K, A, B, C, D).");
		char tarifzone;
		tarifzone = sc.next().charAt(0);
		System.out.println("Sie haben Tarifzone " + tarifzone + " gewählt.");
		int fahrpreis;
		switch (tarifzone) {
		case 'K':
			fahrpreis = 140;
			break;
		case 'A':
			fahrpreis = 230;
			break;
		case 'B':
			fahrpreis = 470;
			break;
		case 'C':
			fahrpreis = 960;
			break;
		case 'D':
			fahrpreis = 1140;
			break;
		default:
			fahrpreis = 0;
		}
		System.out.printf("Bitte zahlen Sie %.2f €%n",fahrpreis / 100.);
		int restgeld = fahrpreis;
		int[] gueltigesGeld = {10, 20, 50, 100, 200, 500, 1000, 2000};
		int einwurf;
		while (true) {
//			restgeld = (int) (restgeld - sc.nextDouble() * 100); 
//			restgeld -= sc.nextDouble() * 100; // bei -= wird der Cast automatisch vom Compiler gemacht
			while (true) {
				einwurf = (int) (sc.nextDouble() * 100);
				if (istEnthalten(einwurf, gueltigesGeld)) {
					break;
				}
				System.out.println("Bitte werfen Sie gültiges Geld ein.");
			}
			restgeld -= einwurf;
			if(restgeld <= 0) {
				break;
			}
			System.out.printf("Noch zu zahlen: %.2f €%n", restgeld / 100.);
		} 
		System.out.println("Das Ticket wird ausgegeben. Gute Fahrt!");
		if (restgeld < 0) {
			restgeld *= -1;
			System.out.printf("Sie bekommen zurück: %.2f €%n", restgeld / 100.0);
			int[] gueltigeMuenzen = {200, 100, 50, 20, 10};
			do {
				for (int muenze : gueltigeMuenzen)  {
					if (muenze <= restgeld) {
						restgeld -= muenze;
						System.out.printf("%.2f €%n", muenze / 100.);
						break;
					}
				}
			} while (restgeld > 0);
		}
		
	}
	
	public static boolean istEnthalten(int zahl, int[] intArray) {
		for (int i : intArray) {
			if (i == zahl) {
				return true;
			}
		}
		return false;
	}

}








