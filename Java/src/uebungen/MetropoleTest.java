package uebungen;

public class MetropoleTest {

	public static void main(String[] args) {
//		Gegeben seien drei Variablen, die Auskunft über Eigenschaften einer bestimmten Stadt geben: 
//			boolean istHaupstadt;
//			int anzahlEinwohner;
//			double steuernProEinwohner;
//
//			Dabei gilt folgendes: 
//			•	istHauptstadt ist genau dann true, wenn die Stadt eine Hauptstadt ist.
//			•	anzahlEinwohner gibt die Anzahl der Einwohner der Stadt an.
//			•	steuernProEinwohner ist die durchschnittliche monatliche Steuerabgabe pro Einwohner und Monat.
//			Wir definieren eine Metropole als eine Stadt, die Hauptstadt ist mit mehr als 100 000 Einwohner oder die mehr 
//			als 200 000 Einwohner hat und im Durchschnitt mindestens 720 000 000 Euro Steuereinnahmen hat. 
		
//		Beispiel 1:
		boolean istHauptstadt = false;
		int anzahlEinwohner = 500_000;
		double steuernProEinwohner = 1000;
		
		boolean istMetropole = istHauptstadt && anzahlEinwohner >= 100_000 || 
				anzahlEinwohner >= 200_000 && anzahlEinwohner * steuernProEinwohner >= 720_000_000;
		System.out.println("Metropole? " + istMetropole);
		
//		Beispiel 2:
		istHauptstadt = false;
		anzahlEinwohner = 1_700_000;
		steuernProEinwohner = 2000;
		
		istMetropole = istHauptstadt && anzahlEinwohner >= 100_000 || 
				anzahlEinwohner >= 200_000 && anzahlEinwohner * steuernProEinwohner >= 720_000_000;
		System.out.println("Metropole? " + istMetropole);
		
//		Beispiel 3:
		istHauptstadt = true;
		anzahlEinwohner = 3_500_000;
		steuernProEinwohner = 1200;
		
		istMetropole = istHauptstadt && anzahlEinwohner >= 100_000 || 
				anzahlEinwohner >= 200_000 && anzahlEinwohner * steuernProEinwohner >= 720_000_000;
		System.out.println("Die Stadt ist " + (istMetropole ? "" : "k") +  "eine Metropole.");


	}

}
