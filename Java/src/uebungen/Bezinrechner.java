package uebungen;

import java.util.Scanner;

public class Bezinrechner {
	
	public static void main(String[] args) {
//		Programmieren Sie einen Benzinrechner, der folgendes leistet: der Anwender kann über 2 Eingaben 
//		die gefahrenen Kilometer und den Verbrauch in Litern angeben. Ausgegeben werden soll der Verbrauch 
//		auf 100 Kilometern. 
//		Eingabe durch den Benutzer
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die gefahrenen Kilometer ein.");
		double gefahreneKm = sc.nextDouble();
		System.out.println("Bitte geben Sie den Verbrauch in Liter ein.");
		double verbrauchLiter = sc.nextDouble();
		
//		Verarbeitung: Berechnung des Verbrauchs auf 100 Kilometer
		double verbrauch100 = verbrauchLiter * 100 / gefahreneKm;
		
//		Ausgabe des Verbrauchs auf 100 Kilometern
		System.out.println("Sie haben bei " + gefahreneKm + " gefahrenen Kilometern " + verbrauchLiter + " Liter verbraucht.");
		System.out.println("Ihr Verbrauch auf 100 Kilometern beträgt " + verbrauch100 + " Liter.");
		
//		format und printf sind identische Methoden
		System.out.format("Sie haben bei %.1f gefahrenen Kilometern %.1f Liter verbraucht.%n", gefahreneKm, verbrauchLiter);
		System.out.printf("Ihr Verbrauch auf 100 Kilometern beträgt %.1f Liter.", verbrauch100);
		
		// Ressource Scanner schlie�en
		sc.close();
	}

}
