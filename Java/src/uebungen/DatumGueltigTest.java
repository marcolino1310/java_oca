package uebungen;

import java.util.Scanner;

import util.MyClass;

public class DatumGueltigTest {

	public static void main(String[] args) {
//		Programmieren Sie eine Funktion datumGueltig, die ein eingebenes Datum auf Gültigkeit überprüft. 
//		Beispiele: datumGueltig(31,6) liefert false, datumGueltig(44,14) liefert false, datumGueltig(28,2) liefert true.
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Tag ein.");
		int tag = sc.nextInt();
		System.out.println("Bitte geben Sie den Monat ein.");
		int monat = sc.nextInt();
		System.out.println("Bitte geben Sie das Jahr ein.");
		int jahr = sc.nextInt();
		
		boolean istGueltig = datumGueltig(tag, monat,jahr);
		System.out.println("istGueltig = " + istGueltig);
	}
	
	public static boolean datumGueltig(int tag, int monat) {
		if (tag < 1) {
			return false;
		}
		boolean istGueltig;
		switch (monat) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			istGueltig = tag <= 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			istGueltig = tag <= 30;
			break;
		case 2:
			istGueltig = tag <= 28;
			break;
		default:
			istGueltig = false;
		}
		return istGueltig;
	}

//	Überladen einer Methode: derselbe Name, aber unterschiedliche Argumente/Parameter
	public static boolean datumGueltig(int tag, int monat, int jahr) {
		if (tag < 1 || tag > 31 || monat < 1 || monat > 12 || jahr < 1900 || jahr > 2099) {
			return false;
		}
		boolean istGueltig;
		int tageFebruar = MyClass.istSchaltjahr(jahr) ? 29 : 28;
		switch (monat) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
//			istGueltig = tag <= 31;
//			break;
			return tag <= 31;
		case 4:
		case 6:
		case 9:
		case 11:
			istGueltig = tag <= 30;
			break;
		case 2:
			istGueltig = tag <= tageFebruar;
			break;
		default:
			istGueltig = false;
		}
		return istGueltig;
	}
}
