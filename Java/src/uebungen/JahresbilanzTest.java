package uebungen;

import java.util.Random;

public class JahresbilanzTest {

	public static void main(String[] args) {
//		Herr Metzger ist Besitzer eines Gemüseladens. Jeden Abend macht er Kassensturz, errechnet den Tagesumsatz und übernimmt ihn in sein Bilanzprogramm, das die Daten wochenweise verwaltet. 
//		Nun, da das Geschäftsjahr zu Ende ist, will er Bilanz ziehen. Schreiben Sie ein Programm, das folgende Funktionen erfüllt: 
//		1. Ein geeignetes Array zur Aufnahme von 52 Kalenderwochen mit je sechs Werten (Sonntag ist geschlossen) definieren. 
//		2. Jedes dieses Felder wird per Zufallsgenerator mit einem Wert zwischen 650 und 1120 (Euro) befüllt. 
//		3. Samstags liegen die Umsätze um 200 Euro höher. 
//		4. Anschließend gibt das Programm die Umsätze wochenweise auf der Konsole aus.

		int[][] umsaetze = new int[52][6];		
		Random r = new Random();
			
		for (int i = 0; i< umsaetze.length; i++) {
			for (int j = 0; j < umsaetze[i].length; j++) {
				umsaetze[i][j] = 650 + r.nextInt(471); // r.nextInt(471) erzeugt Zufallszahlen vom Typ int zwischen 0 inklusive und 471 exklusive
				umsaetze[i][j] = 650 + (int) (Math.random() * 471);
			}
			umsaetze[i][5] += 200; // Samstag ist der Umsatz um 200 Euro höher
		}
		
		int[] wochenUmsaetze = new int[52];
		for (int i = 0; i< umsaetze.length; i++) {
			for (int j = 0; j < umsaetze[i].length; j++) {
				wochenUmsaetze[i] += umsaetze[i][j];
			}
		}

//		for (int umsatz : wochenUmsaetze) {
//			System.out.println(umsatz);
//		}
		
		for (int i = 0; i < wochenUmsaetze.length; i++) {
			System.out.println(i + 1 + ": " + wochenUmsaetze[i]);
		}
		
	}

}
