package uebungen;

public class SortierenTest {

	public static void main(String[] args) {
//		Schreiben Sie eine Java-main-Methode, in der die Werte von drei lokalen int-Variablen a, b und c 
//		aufsteigend sortiert werden. Verwenden Sie als Kontrollstruktur nur die if-Anweisung. 
//		Vertauschen Sie die Werte dieser Variablen so, dass zum Schluss a < b < c gilt. 
//		Prüfen Sie Ihre Methode mit allen Permutationen der Zahlen 1, 2 und 3 (es gibt insgesamt 6 Kombinationen).
//		Nach der Ausführung Ihrer Anweisungen muss in all diesen Fällen also immer a=1, b=2 und b=3 gelten. 
//		Man kann die Aufgabe mit drei if-Anweisungen (ohne else) lösen, da im schlimmsten Fall drei Vertauschungen
//		nötig sind.
		int a = 13;
		int b = 11;
		int c = 21;
		int temp;
		
		if (a > b) {
			temp = b;
			b = a;
			a = temp;
		}
		if (a > c) {
			temp = c;
			c = a;
			a = temp;
		}
		if (b > c)  {
			temp = c;
			c = b;
			b = temp;
		}
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("c = " + c);
		
	}

}
