package uebungen;

public class ReisproblemTest {

	public static void main(String[] args) {
		// Wie viele Jahre braucht man bei einer Reiswelternte von 527,4 Mio. Tonnen, um das Schachbrett
		// zu füllen?
		// Hinweis: 1 Reiskorn wiegt 0,25 Gramm.
		double anzahlReisKoerner = 1;
		double anzahlReisKoernerGesamt = 0;
		for (int i = 1; i <= 64 ; i++) {
			System.out.println(i + ": " + anzahlReisKoerner);
			anzahlReisKoernerGesamt += anzahlReisKoerner;
			anzahlReisKoerner += anzahlReisKoerner;			
		}
		System.out.println("anzahlReiskoernerGesamt = " + anzahlReisKoernerGesamt);
		System.out.printf("Gewicht aller Reiskörner in Gramm: %.0f%n", anzahlReisKoerner * 0.25);
		System.out.println("Gewicht aller Reiskörner in Tonnen: " + anzahlReisKoerner * 0.25 / 1000 / 1000);
		System.out.println("Anzahl der Jahre: " + anzahlReisKoerner * 0.25 / 1000 / 1000 / 527_400_000);

	}

}
