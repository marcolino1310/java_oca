package uebungen.gehalt;

import java.text.NumberFormat;

public class PersonTest {

	public static void main(String[] args) {
//		Bilden Sie in Java eine Klassenhierarchie zur Gehaltsberechnung ab:  
//			 
//		Leiten Sie von einer abstrakten Basisklasse Person die zwei konkreten Unterklassen Angestellter und LeitenderAngestellter ab. 
//		Alle Personen sollen über einen Namen verfügen. Definieren Sie in Person eine abstrakte Methode berechneGehalt(), die in den konkreten 
//		Unterklassen jeweils unterschiedlich implementiert wird: das Gehalt für Angestellte beträgt 2.000 Euro und erhöht sich um 50 Euro je 
//		Überstunde(Attribut der Klasse Angestellter).  Das Gehalt für leitende Angestellte beträgt 3.000 Euro plus Bonus (Attribut der Klasse 
//		LeitenderAngestellter). Leitende Angestellte machen keine Überstunden! Schreiben Sie weiterhin eine Methode, die eine beliebige 
//		Anzahl von Person-Objekten als Parameter erhält, für jede Person jeweils Name und Gehalt sowie die Gesamtsumme der Gehälter aller 
//		Personen ausgibt.  
//			 
//		Setzen Sie die Anforderungen in Java um und implementieren Sie die Methoden. 
//			 
//		Überprüfen Sie Ihre Implementierung mit einem Beispielprogramm mit main-Methode. 
		Person p1 = new Angestellter("Meier", 5);
		Person p2 = new Angestellter("Abel", 7);
		Person p3 = new LeitenderAngestellter("Berger", 150);
		Angestellter a1 = new Angestellter("Fuchs", 0);
		LeitenderAngestellter la1 = new LeitenderAngestellter("Hellwig", 500);
		printPersonen(p1, p2, p3, a1, la1);
	}
	
	public static void printPersonen(Person... personen) {
		int gesamt = 0;
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		for (Person person : personen) {
			System.out.println(person);
			gesamt += person.berechneGehalt();
		}
		System.out.println("Gesamtgehalt: " + nf.format(gesamt));
	}

}
