package uebungen.gehalt;

public class Angestellter extends Person{
	
	private int ueberstunden;
	private static final int grundgehalt;
	
	static {
		grundgehalt = 2000;
	}
	
	{
		ueberstunden = 0;
	}
	
	public Angestellter() {}
	
	public Angestellter(String name, int ueberstunden) {
		super(name);
		this.setUeberstunden(ueberstunden);
	}

	public int getUeberstunden() {
		return ueberstunden;
	}

	public void setUeberstunden(int ueberstunden) {
		if (ueberstunden < 0 || ueberstunden > 40) {
			throw new IllegalArgumentException("Überstunden müssen zwischen 0 und 40 sein: " + ueberstunden);
		}
		this.ueberstunden = ueberstunden;
	}

	@Override
	public int berechneGehalt() {
		return grundgehalt + 50 * ueberstunden;
	}

}
