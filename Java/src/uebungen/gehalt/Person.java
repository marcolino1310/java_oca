package uebungen.gehalt;

import java.text.NumberFormat;

public abstract class Person {
	
	private String name;
	
	public Person() {}
	
	public Person(String name) {
		this.setName(name);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.length() < 2) {
			throw new IllegalArgumentException("Name darf nicht leer sein und muss mindestens 2 Zeichen enthalten: " + name);
		}
		this.name = name;
	}

	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		return this.getClass().getSimpleName() + " " + name + ", Gehalt: " + nf.format(berechneGehalt());
	}
	
	public abstract int berechneGehalt();
}
