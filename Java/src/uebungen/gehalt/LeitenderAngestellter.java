package uebungen.gehalt;

public class LeitenderAngestellter extends Person{
	
	private int bonus;
	
	public LeitenderAngestellter() {}
	
	public LeitenderAngestellter(String name, int bonus) {
		super(name);
		this.setBonus(bonus);
	}
	
	public int getBonus() {
		return bonus;
	}

	public void setBonus(int bonus) {
		if (bonus < 0 || bonus > 1000) {
			throw new IllegalArgumentException("Bonus muss zwischen 0 und 1000 liegen: " + bonus);
		}
		this.bonus = bonus;
	}

	@Override
	public int berechneGehalt() {
		return 3000 + bonus;
	}

}
