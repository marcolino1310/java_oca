package uebungen;

public class FibonacciTest {

	public static void main(String[] args) {
//		Programmieren Sie die Ausgabe der ersten 50 Zahlen der Fibonacci-Folge. Die ersten beiden Zahlen der Fibonacci-Folge sind 0 
//		und 1. Alle weiteren Zahlen ergeben sich aus der Summe der beiden vorangegangenen Zahlen. 
//		Die ersten 10 Zahlen der Fibonacci-Folge sind also: 0 1 1 2 3 5 8 13 21 34
		long zahl1 = 0;
		long zahl2 = 1;
		System.out.printf(" 1: %,15d%n", zahl1);
		System.out.printf(" 2: %,15d%n", zahl2);
		
		long fibo;
		
		for (int i = 3; i <= 50; i++) {
//			Addieren der beiden Vorgänger und Ausgabe der Fibonaccizahl
			fibo = zahl1 + zahl2;
			System.out.printf("%2d: %,15d%n", i, fibo);
//			zahl2 wird nach zahl1 verschoben
			zahl1 = zahl2;
//			fibo wird nach zahl2 verschoben
			zahl2 = fibo;
		}

		
	}

}
