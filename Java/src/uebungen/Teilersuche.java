package uebungen;

public class Teilersuche {

	public static void main(String[] args) {
//		Programmieren Sie eine Funktion teilerSuche, die für Zahlen von 1 bis 100 eine Liste aller Teiler ausgibt. 
//		Beispiel: Die Zahl 66 ist durch 1, 2, 3, 6, 11, 22, 33 und 66 teilbar, also teilerSuche(66) = 1 2 3 6 ... 66
		
		long start = System.currentTimeMillis(); // Anzahl der Millisekunden seit dem 1.1.1970, 0 Uhr GMT
		
//		for (int zahl = 1; zahl <= 50_000; zahl++) {
//			System.out.print(zahl + ": ");
//			for (int i = 1; i <= zahl; i++) {
//				if (zahl % i == 0) {
//					System.out.print(i + " ");
//				}
//			}
//			System.out.println();
//		}
		
		for (int zahl = 1; zahl <= 50_000; zahl++) {
			teilersuche2(zahl);
		}
		
		long stop = System.currentTimeMillis(); // Anzahl der Millisekunden seit dem 1.1.1970, 0 Uhr GMT
		
		System.out.println("Dauer in ms: " + (stop - start));
		
		System.out.println("Servus");

	}
	
	public static void teilersuche1(int zahl) {
		System.out.print(zahl + ": 1 ");
		for (int i = 2; i <= zahl - i; i++) {
			if (zahl % i == 0) {
				System.out.print(i + " ");
			}
		}
		System.out.println(zahl);
	}
	
	public static void teilersuche2(int zahl) {
		System.out.print(zahl + ": ");
		for (int i = 1; i <= zahl / 2; i++) {
			if (zahl % i == 0) {
				System.out.print(i + " ");
			}
		}
		System.out.println(zahl);
	}

}
