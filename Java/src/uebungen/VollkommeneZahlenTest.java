package uebungen;

public class VollkommeneZahlenTest {

	public static void main(String[] args) {
//		Eine vollkommene Zahl ist eine natürliche Zahl, deren Teiler, wenn du sie addierst, 
//		die Zahl selbst ergeben. Da jede Zahl durch sich selbst teilbar ist, darfst du den Teiler, 
//		der die Zahl selbst darstellt, nicht hinzuaddieren.
//		Beispiel: 6 = 1 + 2 + 3
//		Ermitteln Sie weitere vollkommene Zahlen.
		for (int zahl = 1; zahl < Integer.MAX_VALUE; zahl++) {
			int summeDerTeiler = 0;
			for (int i = 1; i <= zahl/2; i++) {
				if (zahl % i == 0) {
					summeDerTeiler += i;
				}
			}
			if (zahl == summeDerTeiler) {
				System.out.println(zahl + " ist eine vollkommene Zahl.");
			}
		}
	}

}
