package uebungen;

public class ArrayVergleichTest {

	public static void main(String[] args) {
//		Erzeugen Sie zwei int-Arrays zahlen1 und zahlen2 mit jeweils 10 zufälligen Elementen von 0 bis 50. Lassen Sie sich beide Arrays ausgeben. 
		 
//		Vergleichen Sie beide Arrays derart, dass entweder das erste gemeinsame Element ausgegeben wird (sofortiger Schleifenabbruch) oder ausgegeben wird, dass es keine gemeinsamen Elemente gibt.  
		 
//		Wie oft kommt es vor, dass die beiden Arrays mindestens ein gemeinsames Element haben? Erzeugen Sie eine geeignete Simulation mit Hilfe einer Schleife. 
		
		final int ANZAHL = 1_000_000;
		int treffer = 0;
		
		for (int j = 0; j < ANZAHL; j++) {
			int[] zahlen1 = new int[10];
			int[] zahlen2 = new int[10];
			for (int i = 0; i < zahlen1.length; i++) {
				zahlen1[i] = (int) (Math.random() * 51);
			}
			for (int i = 0; i < zahlen2.length; i++) {
				zahlen2[i] = (int) (Math.random() * 51);
			}
//			for (int zahl : zahlen1) {
//				System.out.println(zahl);
//			}
//			System.out.println("*********");
//			for (int zahl : zahlen2) {
//				System.out.println(zahl);
//			}
//			System.out.println("*********");
			outer: for (int zahl1 : zahlen1) {
				for (int zahl2 : zahlen2) {
					if (zahl1 == zahl2) {
//						System.out.println(zahl1);
						treffer++;
						break outer;
					}
				}
			} 
		}
		System.out.println("Treffer in Prozent: " + 100. * treffer / ANZAHL);
	}

}
