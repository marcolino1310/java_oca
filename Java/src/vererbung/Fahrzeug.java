package vererbung;

public class Fahrzeug {
	
	// Variablen
	private String hersteller;
	private String modell;
	private int ps;
	
	// Konstruktoren
	public Fahrzeug() {}
	
	public Fahrzeug(String hersteller, String modell, int ps) {
		setHersteller(hersteller);
		setModell(modell);
		setPs(ps);
	}

	// getter und setter
	public String getHersteller() {
		return hersteller;
	}

	public void setHersteller(String hersteller) {
//		Hersteller darf nicht null sein. Hersteller muss mindestens 2 Zeichen lang sein
		if (hersteller == null || hersteller.length() < 2) {
			throw new IllegalArgumentException("Hersteller darf nicht null sein und muss mindestens 2 Zeichen enthalten: " + hersteller);
		}
		this.hersteller = hersteller;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		if (modell == null || modell.length() < 1) {
			throw new IllegalArgumentException("Modell darf nicht null sein und muss mindestens 1 Zeichen enthalten: " + modell);
		}
		this.modell = modell;
	}

	public int getPs() {
		return ps;
	}

	public void setPs(int ps) {
		if (ps < 50 || ps > 300) {
			throw new IllegalArgumentException("PS muss zwischen 50 und 300 liegen: " + ps);
		}
		this.ps = ps;
	}
	
//	Weitere Methoden
//	Um zu gewährleisten, dass eine Methode korrekt überschrieben wird, kann man eine Annotation verwendet
	@Override
	public String toString() {
		return hersteller + " " + modell + ", " + ps + " PS";
	}
	

}
