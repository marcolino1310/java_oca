package vererbung;

public class PersonenFahrzeug extends Fahrzeug{
	
	private int geschwindigkeit;
	
	public PersonenFahrzeug() {}
	
	public PersonenFahrzeug(String hersteller, String modell, int ps, int geschwindigkeit) {
		super(hersteller, modell, ps);
		this.setGeschwindigkeit(geschwindigkeit);
	}

	public int getGeschwindigkeit() {
		return geschwindigkeit;
	}

	public void setGeschwindigkeit(int geschwindigkeit) {
		if (geschwindigkeit < 150 || geschwindigkeit > 300) {
			throw new IllegalArgumentException("Geschwindigkeit muss zwischen 150 und 250 km/h liegen: " + geschwindigkeit);
		}
		this.geschwindigkeit = geschwindigkeit;
	}
	
	public String toString() {
		return super.toString() + ", " + geschwindigkeit + " km/h";
	}

}
