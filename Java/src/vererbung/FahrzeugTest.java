package vererbung;

public class FahrzeugTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Fahrzeug fahrzeug1 = new Auto("Ford", "Focus", 200, 5, 210); // Hersteller, Modell, PS, Anzahl Türen, Geschwindigkeit
		Fahrzeug fahrzeug2 = new Motorrad("Suzuki", "Bandit 600 S", 90, true, 220); // Hersteller, Modell, PS, verkleidet (ja/nein), Geschwindigkeit
		Fahrzeug fahrzeug3 = new LKW("Mercedes", "Atego", 250, 7.5); // Hersteller, Modell, PS, Zuladung
		Fahrzeug fahrzeug4 = new Auto("Kia", "X", 100, 5, 200); // Hersteller, Modell, PS, Anzahl Türen, Geschwindigkeit
		
//		Ausgabe aller Variablen
		System.out.println(fahrzeug1);
		System.out.println(fahrzeug2);
		System.out.println(fahrzeug3);
		System.out.println(fahrzeug4);
		
//		VIER Klassen, SIEBEN Variablen, DREI extents

	}

}
