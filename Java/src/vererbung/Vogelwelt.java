package vererbung;

public class Vogelwelt {

	public static void main(String[] args) {
//		Vogel henne = new Vogel(); // Compiler-Fehler, da Klasse Vogel abstract
		FlugVogel henne = new FlugVogel();
		henne.fressen();
		LaufVogel emu = new LaufVogel();
		emu.fressen();
		FlugVogel adler = new FlugVogel();
//		henne.fliegen(); // Compiler-Fehler. da fliegen-Methode nicht in Klasse Vogel, sondern FlugVogel
		adler.fliegen();
		henne.setGroesse(35);
		henne.setGeschlecht('w');
		emu.setGroesse(160);
		emu.setGeschlecht('w');
		adler.setGroesse(80);
		adler.setGeschlecht('m');
//		henne.tempo = 10; // Compiler-Fehler, da tempo in LaufVogel
		emu.tempo = 60;
//		adler.tempo = 50; // Compiler-Fehler, da tempo in LaufVogel
//		emu.fliegen(); // Compiler-Fehler, da fliegen nur in FlugVogel
		adler.fortpflanzen();
		System.out.println("*******");
		henne.fortpflanzen();
		Kuckuck kuckuck = new Kuckuck();
		System.out.println("*******");
		kuckuck.fortpflanzen();
//		SuedAmerikanischerKuckuck kuckuck2 = new SuedAmerikanischerKuckuck();
//		System.out.println("*******");
//		kuckuck2.fortpflanzen();		
		LaufVogel strauss = new LaufVogel(170, 'm', 55);
		Amsel amsel = new Amsel(20, 'w');
		amsel.fressen();
		Wurm wurm = new Wurm();
		amsel.fressen(wurm);
		kuckuck.fressen();
//		kuckuck.fressen(wurm); // Compiler-Fehler, da fressen mit Wurm NUR in Klasse Amsel definiert
		Korn korn = new Korn();
		amsel.fressen(korn);
		amsel.fressen(new Korn());
//		strauss.setGeschlecht('d');
		System.out.println(amsel); // eigentlich: System.out.println(amsel.toString());
		System.out.println(strauss.toString()); // Ich bin 170 cm groß und habe Geschlecht m. Ich bin 55 km/h schnell.
		System.out.println(adler);
		System.out.println(emu);
		System.out.println(kuckuck); // Standard 20 cm und weiblich
		
		System.out.println("**************");
		info(kuckuck);
		info(strauss);
		info(amsel);
	}

	public static void info(Vogel v) {
		System.out.println(v.toString());
	}



}
