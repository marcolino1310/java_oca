package vererbung;

// Abstrakte Basisklasse/Superklasse
public abstract class Vogel extends Object { // extends Objekts steht dort implizit, wenn kein extends angegeben wird

	private int groesse;
	private char geschlecht;
	 
	public Vogel() {
		this(20, 'w');
	}
	
	public Vogel(int groesse, char geschlecht) {
		super(); // impliziter Aufruf des Standardkonstruktors der Superklasse
		setGroesse(groesse);
		setGeschlecht(geschlecht);
	}
	
	public int getGroesse() {
		return groesse;
	}

	public void setGroesse(int groesse) {
		if (groesse <= 0) {
			throw new IllegalArgumentException("groesse muss positiv sein: " + groesse);
		}
		this.groesse = groesse;
	}

	public char getGeschlecht() {
		return geschlecht;
	}

	public void setGeschlecht(char geschlecht) {
//		if (geschlecht != 'w' && geschlecht != 'm') {
		if (!(geschlecht == 'm' ^ geschlecht == 'w')) {
			throw new IllegalArgumentException("gechlecht muss m oder w sein: " + geschlecht);

		}
		this.geschlecht = geschlecht;
	}

	public void fressen() {
		System.out.println("Allgemeines Fressen");
	}
	
	public void fortpflanzen() {
		System.out.println("Nest bauen");
		System.out.println("Eier legen");
		System.out.println("Eier brüten");
		System.out.println("Aufziehen");
	}
	
//	Überschreiben der geerbten toString-Methode aus Object
	public String toString() {
		return "Ich bin " + groesse + " cm groß und ich habe das Geschlecht " + geschlecht + ".";
	}
}








