package vererbung;

public final class Kuckuck extends FlugVogel{
	
	public Kuckuck() {
//		super(); // implizit
	}
	
	public Kuckuck(int groesse, char geschlecht) {
		super(groesse, geschlecht);
	}
	
	// Die Klasse Kuckuck erbt zwei Variablen un drei Methoden
	// Die geerbte Methode fortpflanzen muss aber anders definiert werden	
	// Überschreiben (Override) einer geerbten Methode
	public void fortpflanzen() {
		System.out.println("Eier legen");
	}
}
