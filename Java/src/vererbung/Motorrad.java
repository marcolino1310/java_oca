package vererbung;

public class Motorrad extends PersonenFahrzeug {
	
	private boolean verkleidet;
	
	public Motorrad() {}

	public Motorrad(String hersteller, String modell, int ps, boolean verkleidet, int geschwindigkeit) {
		super(hersteller, modell, ps, geschwindigkeit);
		this.setVerkleidet(verkleidet);
	}

//	getter einer boolean Variable darf auch isVariable anstatt getVariable heißen
	public boolean isVerkleidet() {
		return verkleidet;
	}

	public void setVerkleidet(boolean verkleidet) {
		this.verkleidet = verkleidet;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", " + (verkleidet ? "verkleidet" : "naked");
	}

}
