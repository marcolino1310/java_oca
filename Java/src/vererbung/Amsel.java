package vererbung; 

public final class Amsel extends FlugVogel{
	
	public Amsel() {}
	
	public Amsel(int groesse, char geschlecht) {
		super(groesse, geschlecht);
	}
	
//	Definition einer neuen Methode mit demselben Namen wie eine geerbte Methode => Überladen einer Methode
	public void fressen(Wurm wurm) {
		System.out.println("Amseln lieben Würmer.");
	}
	
	public void fressen(Korn korn) {
		System.out.println("Körner sind auch gut.");
	}

}

class Wurm {}
class Korn {}