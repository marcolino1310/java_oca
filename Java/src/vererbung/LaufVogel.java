package vererbung;

public class LaufVogel extends Vogel {
	int tempo;

	public LaufVogel() {
	}

	public LaufVogel(int groesse, char geschlecht, int tempo) {
		super(groesse, geschlecht); // Aufruf des Super-Konstruktors, Konstruktor in der Super-Klasse Vogel
//		this.groesse = groesse;
//		this.geschlecht = geschlecht;
		this.tempo = tempo;
	}
	
//	Überschreiben der von Vogel geerbten toString-Methode
	public String toString() {
//		return "Ich bin " + getGroesse() + " cm groß. Ich habe das Geschlecht " + getGeschlecht() + "." + " Ich bin " + tempo + " km/h schnell.";
//		Aufruf der geerbten Methode aus der Superklasse
		return super.toString() + " Ich bin " + tempo + " km/h schnell.";
	}
}
