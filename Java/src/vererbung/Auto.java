package vererbung;

public class Auto extends PersonenFahrzeug{
	
	private int anzahl_tueren;
	
	public Auto() {}
	
	public Auto(String hersteller, String modell, int ps, int anzahl_tueren, int geschwindigkeit) {
		super(hersteller, modell, ps, geschwindigkeit);
		this.setAnzahl_tueren(anzahl_tueren);
	}

	public int getAnzahl_tueren() {
		return anzahl_tueren;
	}

	public void setAnzahl_tueren(int anzahl_tueren) {
		if (anzahl_tueren < 3 || anzahl_tueren > 7) {
			throw new IllegalArgumentException("Anzahl der Türen muss zwischen 3 und 7 liegen: " + anzahl_tueren);
		}
		this.anzahl_tueren = anzahl_tueren;
	}
	
	public String toString() {
		return super.toString() + ", " + anzahl_tueren + " Türen";
	}
	
	
	
}
