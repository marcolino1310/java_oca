package vererbung;

public class LKW extends Fahrzeug{
	
	private double zuladung;
	
	public LKW() {}
	
	public LKW(String hersteller, String modell, int ps, double zuladung) {
		super(hersteller, modell, ps);
		this.setZuladung(zuladung);
	}

	public double getZuladung() {
		return zuladung;
	}

	public void setZuladung(double zuladung) {
		if (zuladung < 7.5 || zuladung > 40) {
			throw new IllegalArgumentException("Zuladung muss zwischen 7,5 und 40 Tonnen liegen: " + zuladung);
		}
		this.zuladung = zuladung;
	}

	@Override
	public String toString() {
		return super.toString() + ", Zuladung: " + zuladung + " t";
	}
	
}
