package vererbung;

public class FlugVogel extends Vogel{
	
	public FlugVogel() {
		// super(); // impliziter Aufruf immer die erste Anweisung im Konstruktor außer man schreibt explizit super oder this
	}
	
	public FlugVogel(int groesse, char geschlecht) {
		super(groesse, geschlecht);
	}

	public void fliegen() {
		System.out.println("fliiiiiiiiieeeeeeg");
	}
}
