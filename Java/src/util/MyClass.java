package util;

public class MyClass {

	public static boolean istSchaltjahr(int jahr) {
//		if (jahr % 4 == 0 && jahr % 100 != 0 || jahr % 400 == 0) {
//			return true;
//		} else {
//			return false;
//		}
		return jahr % 4 == 0 && jahr % 100 != 0 || jahr % 400 == 0;
	}
	
	public static double berechneEndkapital(double startkapital, double zinssatz, double laufzeit) {
		return startkapital * Math.pow(1 + zinssatz / 100, laufzeit);
	}
	
	public static long berechneFakultaet(int zahl) {
		long fakultaet = 1;

		for (int i = 1; i <= zahl; i++) {
			fakultaet *= i;
		}
		return fakultaet;
	}
	
	public static long berechneFakultaetRekursiv(int n) {
		if (n == 0) {
			return 1;
		}
		return n * berechneFakultaetRekursiv(n - 1);
	}

	public static long berechneFibonacci(int zahl) {
		if (zahl == 1) {
			return 0;
		}
		if (zahl == 2) {
			return 1;
		}
		return berechneFibonacci(zahl - 2) + berechneFibonacci(zahl - 1);
	}
	
	public static int berechneDurchmesser(int radius) {
		return 2 * radius;
	}
	
	public static double berechneUmfang(int radius) {
		return 2 * radius * Math.PI;
	}
	
	

}














