package basics;

public class VarArgsTest {

	public static void main(String[] args) {
		int zahl1 = 42;
		int zahl2 = 77;
		int zahl3 = 12;
		int zahl4 = 55;
		int zahl5 = 23;
		int summe = berechneSumme(zahl1, zahl2);
		System.out.println("summe = " + summe);
		summe = berechneSumme(zahl1, zahl2, zahl3);
		System.out.println("summe = " + summe);
		int[] zahlen = { zahl1, zahl2, zahl3, zahl4, zahl5 };
//		summe = berechneSumme(zahlen);
//		System.out.println("summe = " + summe);
		summe = berechneSumme(zahl1, zahl2, zahl3, zahl4, zahl5, 77, 123, 456);
		System.out.println("summe = " + summe);
//		summe = berechneSumme(zahl1); // Compiler-Fehler: mindestens 2 int!
//		System.out.println("summe = " + summe);
//		summe = berechneSumme(); // Compiler-Fehler: mindestens 2 int!
//		System.out.println("summe = " + summe);

	}

	public static int berechneSumme(int zahl1, int zahl2) {
		System.out.println("berechneSumme mit zahl1 und zahl2");
		return zahl1 + zahl2;
	}

	public static int berechneSumme(int zahl1, int zahl2, int zahl3) {
		return zahl1 + zahl2 + zahl3;
	}
	
//	Mischen von "normalen" Argument und var-args
//	Reglen: es darf immer nur EINEN var-args-Argument geben; dieses muss an der letzten Stelle stehen
//	Es müssen mindestens zwei int-Variablen übergeben werden (2 bis n)
	public static int berechneSumme(int zahl1, int zahl2, int... zahlen) {
		System.out.println("berechneSumme var-args");
		int summe = zahl1 + zahl2;
		for (int i : zahlen) {
			summe += i;
		}
		return summe;
	}
	
//	Mischen von "normalen" Argument und var-args
//	Reglen: es darf immer nur EINEN var-args-Argument geben; dieses muss an der letzten Stelle stehen
//	Es muss mindestens eine int-Variable übergeben werden (1 bis n)
//	public static int berechneSumme(int zahl, int... zahlen) {
//		System.out.println("berechneSumme var-args");
//		int summe = zahl;
//		for (int i : zahlen) {
//			summe += i;
//		}
//		return summe;
//	}

//	Variable Argumentenliste (var-args)
//	Es können beliebig viele int-Variablen übergeben (0 bis n)
//	public static int berechneSumme(int... zahlen) {
//		System.out.println("berechneSumme var-args");
//
//		int summe = 0;
//		for (int i : zahlen) {
//			summe += i;
//		}
//		return summe;
//	}

//	public static int berechneSumme(int[] intArr) {
//		int summe = 0;
//		for (int i : intArr) {
//			summe += i;
//		}
//		return summe;
//	}

}
