package basics;

public class ScopingTest {
	
//	3. Objektvariable
//	Ist so lange gültig, wie das Objekt "lebt"
	private String name = "Hallo";
	
//	4. Klassenvariable
//	Ist so lange gültig, wie die Klasse geladen ist
	private static int anzahl = 0;

	public static void main(String[] args) {
//		int zahl = 77; // Compiler-Fehler in Zeile 12 => keine Blockvariable mit demselben Namen erlaubt

		// Der Scope eine Variablen gibt an, in welchem Bereich sie gültig ist und wie lange sie "lebt"
		// 4 Arten von Variablen
		// 1. Blockvariable
		if (Math.random() > 0.5) {
			int zahl = 42; // Blockvariable
			System.out.println(zahl);
		}
//		System.out.println(zahl); // Compiler-Fehler: außerhalb der Gütligkeit (Scope) von zahl
		// 2. Lokale Variable (wird innerhalb einer Methode deklariert)
		int zahl = 77;
		System.out.println(zahl);
		
		testX();
//		System.out.println(x); // Compiler-Fehler: kein Zugriff auf lokale Variablen in anderen Methoden
		ScopingTest scop = new ScopingTest();
		System.out.println(scop.name);
		scop = new ScopingTest();
		 
		System.out.println(anzahl);
	}
	
	public static void testX() {
		int x = 42; // Lokale Variable
		x++;
		testY();
//		System.out.println(y); // Compiler-Fehler: kein Zugriff auf lokale Variablen in anderen Methoden
		System.out.println(x);
		System.out.println(anzahl);

	}
	
	public static void testY() {
		int y = 77;
		System.out.println(y);
	}

}
