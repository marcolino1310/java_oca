package basics;

import util.MyClass;

public class CastingTest {

	public static void main(String[] args) {
		int intVar = 42;
		long longVar = 77;
		longVar = intVar; // Compiler führt die Typumwandlung (cast) von int auf long automatisch (implizit) durch (widening)
//		intVar = longVar; // Compiler-Fehler
		intVar = (int) longVar; // Explizite Typumwandlung
		System.out.println("intVar = " + intVar);
		
		longVar = 7_987_654_321L; // Ganzzahl-Literale werden als int gespeichert. Mit dem L werden sie als long gespeichert.
		intVar = (int) longVar; // Explizite Typumwandlung
		System.out.println("intVar = " + intVar);
		
		longVar = 2_147_483_648L; // Ganzzahl-Literale werden als int gespeichert. Mit dem L werden sie als long gespeichert.
		intVar = (int) longVar; // Explizite Typumwandlung
		System.out.printf("intVar = %,d%n", intVar);
		
		double pi = 3.14; // Fließkommazahl-Literale werden als double (8 byte) gespeichert.
		float f = 2.71f; // Literal wird als float gespeichert
//		pi = intVar;
		intVar = (int) pi;
		System.out.printf("intVar = %,d%n", intVar);
		
//		Arithmetische Operationen haben immer mindestens int als Typ, sonst immer Typ des "größten" Operanden
 		int augenzahl =  (int) (Math.random() * 6 + 1); // Zufallszahlen von [1, 7[
 		System.out.println("augenzahl = " + augenzahl);
 		
 		int endkapital = (int) MyClass.berechneEndkapital(1000, 5, 10);
 		System.out.println("endkapital = " + endkapital);
 		
// 		Zusammenhang float - long
 		f = longVar;
// 		longVar = f; // Compiler-Fehler
 		
 		int i = 9812;
 		System.out.println("i = " + i);
 		System.out.println("i = " + (char) i);
 		char c = 'Z';
 		System.out.println("c = " + c);
 		System.out.println("c = " + (int) c);

	}
}






