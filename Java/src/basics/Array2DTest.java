package basics;

import java.util.Scanner;

public class Array2DTest {

	public static void main(String[] args) {
//		Umsatzzahlen aus den Jahren 2017 bis 2019 pro Quartal
//		int[][] umsaetze = new int[3][]; 1 2D-Array, kein 1D-Array
		int[][] umsaetze = new int[3][4]; 
//		System.out.println(umsaetze[0][1]); // Umsatz Q2 von 2017
//		System.out.println(umsaetze[2][3]); // Umsatz Q4 von 2019
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Umsätze ein.");
//		for-Schleife zur Iteration über ein 2D-Array
		for (int i = 0; i < umsaetze.length; i++) {
			for (int j = 0; j < umsaetze[i].length; j++) {
				System.out.println(2017 + i + " Q" + (j + 1));
//				umsaetze[i][j] = sc.nextInt();
				umsaetze[i][j] = 250;
			}
		}
		int gesamtUmsatz = 0;
//		Iteration mit erweiterter Schleife
		for (int[] umsaetzeJahr : umsaetze) {
			for (int umsatz : umsaetzeJahr) {
				gesamtUmsatz += umsatz;
			}
		}
		System.out.println("gesamtUmsatz = " + gesamtUmsatz);
		
		int[][] noten = new int[4][];
		int[] notenA = {2, 3, 4, 5, 4, 3, 2};
		int[] notenB = {1, 2, 3, 2, 1, 2};
		int[] notenC = {4, 5, 4, 5, 4, 3, 4, 5};
		int[] notenD = {2, 4, 4, 4, 4};
		
		noten[0] = notenA;
		noten[1] = notenB;
		noten[2] = notenC;
		noten[3] = notenD;
		
		int summe = 0;
		int anzahl = 0;
		for (int[] klasse : noten) {
			for (int note : klasse) {
				summe += note;
				anzahl++;
			}
		}
		
		System.out.println("Durchschnittsnote: " + 1. * summe / anzahl);
		
	}

}
