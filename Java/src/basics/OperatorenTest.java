package basics;

public class OperatorenTest {

	public static void main(String[] args) {
		int zahl1 = 42;
		int zahl2 = 12;
//		Der "+"-Operator hat in Java zwei Bedeutungen: sind die beiden Operanden Zahlen, dann bedeutet "+" Addition
//		Ist mindestens einer der beiden Operanden ein String (Zeichenkette), dann bedeuet "+" Verkettung (Konkatenierung)
		System.out.println("zahl1 + zahl2 = " + zahl1 + zahl2);
		System.out.println("zahl1 + zahl2 = " + (zahl1 + zahl2));
		System.out.println("zahl1 * zahl2 = " + zahl1 * zahl2); // Punkt- vor Strichrechnung
		System.out.println("zahl1 / zahl2 = " + zahl1 / zahl2); // Ganzzahl-Division
		System.out.println("zahl1 / zahl2 = " + 1.0 * zahl1 / zahl2);

		int zahl = 7;
		int summe = 55;
		summe = summe + zahl;
		summe += zahl; 
		int produkt = 14;
		produkt = produkt * zahl;
		produkt *= zahl;
		
		zahl = zahl + 1;
		zahl += 1;
		System.out.println("zahl = " + zahl);
		zahl++; //post-inkrement (Operator NACH dem Operanden)
		System.out.println("zahl = " + zahl);
		++zahl; // pre-inkrement (Operator VOR dem Operanden)
		System.out.println("zahl = " + zahl);
		
//		post-inkrement
//		zuerst Operation, dann inkrement
		zahl = 42;
		System.out.println("zahl++ = " + zahl++); // 42
		System.out.println("zahl nach Inkrement = " + zahl); // 43
		
//		pre-inkrement
//		erst inkrement, dann Operation
		zahl = 42;
		System.out.println("++zahl = " + ++zahl); // 43
		System.out.println("zahl nach Inkrement = " + zahl); // 43
		
//		post-dekrement
//		zuerst Operation, dann inkrement
		zahl = 42;
		System.out.println("zahl-- = " + zahl--); // 42
		System.out.println("zahl nach Inkrement = " + zahl); // 43
		
//		pre-dekrement
//		erst inkrement, dann Operation
		zahl = 42;
		System.out.println("--zahl = " + --zahl); // 41
		System.out.println("zahl nach Inkrement = " + zahl); // 43
		
		boolean b1 = true;
		boolean b2 = false;
		boolean b3 = true;
		
		boolean b = b1 && b2 || b3; // true
		System.out.println("b = " + b);
		
		b1 = false;
		b2 = false;
		b3 = true;

		b = b1 && b2 || b3; // UND vor ODER: true
		System.out.println("b = " + b);
		
		b1 = false;
		b2 = false;
		b3 = true;

		b = b1 && b2 || b1 && b3; // UND vor ODER: false
		b = b1 && b3 || b2 && b3; // false
		b = !b1 && b3 || b2 && b3; // NOT vor UND vor ODER: true
		System.out.println("b = " + b);
		
		
	}

}









