package basics;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayTest {

	public static void main(String[] args) {
		int[] zahlen = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int zahl : zahlen) {
			System.out.println(zahl);
		}
//		amerikanische Jahreszeiten
		String[] seasons = {"spring", "summer", "fall", "winter"};
		for (String season : seasons) {
			System.out.println(season);
		}
//		spanische Jahreszeiten mit anonymen Array
		seasons = new String[]{"primavera","verano","otono","invierno"};
		for (String season : seasons) {
			System.out.println(season);
		}
		
		Scanner sc = new Scanner(System.in);
		int quartal1, quartal2, quartal3, quartal4;
		System.out.println("Bitte geben Sie die Umsätze ein.");
//		quartal1 = sc.nextInt();
//		quartal2 = sc.nextInt();
//		quartal3 = sc.nextInt();
//		quartal4 = sc.nextInt();
//		int summe = quartal1 + quartal2 + quartal3 + quartal4;
//		System.out.println("summe = " + summe);

//		Deklaration eines Arrays (Feld)
//		int[] quartalsUmsaetze;
//		Deklaration und Erzeugung eines Arrays (Feld) mit 4 Elementen (int-Variablen)
		int[] quartalsUmsaetze = new int[4];
//		quartalsUmsaetze[0] = sc.nextInt(); // Array-Index beginnt mit 0!
//		quartalsUmsaetze[1] = sc.nextInt(); 
//		quartalsUmsaetze[2] = sc.nextInt(); 
//		quartalsUmsaetze[3] = sc.nextInt(); 
//		Zuweisung der Werte
//		quartalsUmsaetze.length entspricht der Anzahl der Elemente des Arrays 
		System.out.println("Ausgabe der Array-Elemente");
		for (int i = 0; i < quartalsUmsaetze.length; i++) {
			System.out.println(quartalsUmsaetze[i]);
		}

		for (int i = 0; i < quartalsUmsaetze.length; i++) {
//			quartalsUmsaetze[i] = sc.nextInt();
			quartalsUmsaetze[i] = 250;
		}
//		Berechnung der Summe
		int summe = 0;
		for (int i = 0; i < quartalsUmsaetze.length; i++) {
			summe = quartalsUmsaetze[i] + summe;
		}
		System.out.println("summe = " + summe);

		System.out.println("Ausgabe der Array-Elemente");
		for (int i = 0; i < quartalsUmsaetze.length; i++) {
			System.out.println(quartalsUmsaetze[i]);
		}

		System.out.println("Ausgabe der Array-Elemente mit der erweiterten for-Schleife (for-each-Schleife)");
		for (int umsatz : quartalsUmsaetze) {
			if (umsatz < 200) {
				System.out.println("Warnung: " + umsatz);
			} else {
				System.out.println(umsatz);
			}
		}

//		Simulation der Ziehung der Lottozahlen
		int[] lottozahlen = new int[6]; // Java-style
//		int lottozahlen[] = new int[6]; // C-style
//		Ziehung der Lottozahlen
		int temp;
		for (int i = 0; i < lottozahlen.length; i++) {
			do {
				temp = 1 + (int) (Math.random() * 49);
			} while (istEnthalten(temp, lottozahlen));
			lottozahlen[i] = temp;
		}

		Arrays.sort(lottozahlen); // Sortieren des Arrays

		System.out.println("Lottozahlen:");
//		Ausgabe der Lottozahlen
		for (int lottozahl : lottozahlen) {
			System.out.println(lottozahl);
		}
	}

	public static boolean istEnthalten(int zahl, int[] intArray) {
		for (int i : intArray) {
			if (i == zahl) {
				return true;
			}
		}
		return false;
	}

}
