package basics;

public class FiboArray {

	public static void main(String[] args) {
		final int ANZAHL = 50; // Konstanten (final Variablen) werden laut Vereinbarung komplett GROSS geschrieben

		long[] fiboArr = new long[ANZAHL];

//		for(long fibo : fiboArr) {
//			fibo = 42; // Erweiterte Schleife (for-each) kann nicht zur Initialisierung verwendet werden, da fibo nur eine Kopie des Array-Elements ist
//			System.out.println(fibo);
//		}

//		1. Fibonacci-Zahl ist 0: fiboArr[0] = 0 ist eigentlich schon gegegeben
		fiboArr[0] = 0;
//		2. Fibonacci-Zahl ist 1
		fiboArr[1] = 1;
		
		for (int i = 2; i < ANZAHL; i++) {
			fiboArr[i] = fiboArr[i - 1] + fiboArr[i - 2];
		}
		
//		for(long fibo : fiboArr) {
//			System.out.println(fibo);
//		}

		for (int i = 0; i < ANZAHL; i++) {
//			System.out.println(i + 1 + ": " + fiboArr[i]);
			System.out.printf("%2d: %,13d %n", i + 1, fiboArr[i]);
		}
	}

}
