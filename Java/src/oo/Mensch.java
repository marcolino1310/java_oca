package oo;

import java.time.LocalDate;

public class Mensch {
//	Variablen für den Vornamen und das Alter und das Geburtsdatum
	private String vorname;
	private int alter;
	private int geburtstag;
	private int geburtsmonat;
	
//	getter und setter-Methoden
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String v) {
		vorname = v;
	}
	public int getAlter() {
		return alter;
	}
	public void setAlter(int a) {
		alter = a;
	}
	public int getGeburtstag() {
		return geburtstag;
	}
	public void setGeburtag(int tag) {
		geburtstag = tag;
	}
	public int getGeburtsmonat() {
		return geburtsmonat;
	}
	public void setGeburtsmonat(int monat) {
		geburtsmonat = monat;
	}
	
//	1 Methode zum Vorstellen (vgl. info aus  Kreis)
	public void info() {
		System.out.println("Hallo, ich bin " + vorname + " und " + alter + " Jahre alt.");
		System.out.println("Ich habe heute " + (!hatGeburtstag() ? "nicht " : "") +  "Geburtstag.");
	}
	
	public boolean hatGeburtstag() {
		return LocalDate.now().getDayOfMonth() == geburtstag && LocalDate.now().getMonthValue() == geburtsmonat;
	}
}
