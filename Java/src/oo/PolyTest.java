package oo;

public class PolyTest {

	public static void main(String[] args) {
		Lebewesen l1 = new Lebewesen();
		l1.sayHello();
		System.out.println(l1.a);
		System.out.println(Lebewesen.b);
		l1.doItStatic();
		Lebewesen l2 = new Tier();
		l2.sayHello(); // Late binding (späte Bindung) gibt es NUR bei Objektmethoden (Instanzmethoden)
		System.out.println(l2.a);
		System.out.println(l2.b);
		Lebewesen.doItStatic();
		Lebewesen l3 = new Fisch();
		System.out.println("*************");
		print(l1, l2, l3);
	}

	public static void print(Lebewesen... lebewesen) {
//		Für alle Lebewesen sayHello aufrufen; bei Tieren zusätzlich fressen aufrufen
		for (Lebewesen l : lebewesen) {
			l.sayHello();
//			l.fressen(); // Compiler-Fehler, weil fressen nicht in der Klasse Lebewesen definiert ist, sondern in der Subklasse Tier
//			Lösung: Typumwandlung (Cast) von Lebewesen zu Tier (downcast)
//			Typumwandlung nur dann, wenn l zur Laufzeit auf ein Tier referenziert
//			instanceof-Operator überprüft, ob die Referenzvariable (linke Seite) zur Laufzeit auf ein Objekt des Typs (rechte Seite) referenziert
			if (l instanceof Tier) {
//				Tier t = (Tier) l;
//				t.fressen();
				((Tier) l).fressen();
			}
		}
	}

}

class Lebewesen {

	int a = 42;
	static int b = 43;

	public void sayHello() {
		System.out.println("Ich bin ein Lebewesen.");
	}

	public static void doItStatic() {
		System.out.println("doItStatic Lebewesen");
	}
}

class Tier extends Lebewesen {

	int a = 77;
	static int b = 78;

	public void sayHello() {
		System.out.println("Ich bin ein Tier.");
	}

	public void fressen() {
		System.out.println("fressen");
	}

	public static void doItStatic() {
		System.out.println("doItStatic Tier");
	}
}

class Fisch extends Tier {
	
	public void sayHello() {
		System.out.println("Ich bin ein Fisch.");
	}

}
