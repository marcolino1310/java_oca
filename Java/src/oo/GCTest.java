package oo;

public class GCTest {

	public static void main(String[] args) {
//		Objekte, die von außen (Stack) nicht mehr erreicht (referenzierbar) sind, können von der Garbage Collection gelöscht werden
//		Die Garbage Collection von Objekten kann nicht erzwungen (forced) werden
//		Es ist aber gewährleistet, dass die Garbage Collection Objekte löscht, bevor der Heap überläuft
		Object o1 = new Object();
		Object o2 = new Object();
		Object o3 = new Object();
		
		o1 = o2;
		o3 = o1;
		o3 = null;
		o2 = null;
		o1 = null;
		
		System.gc(); // Damit KANN die Garbage Collection angestoßen werden.
	}

}
