package oo;

public class Rechteck {
	
	// Variablen/Eigenschaften eines Rechtecks
	private double a;
	private double b;
	
	// Zählvariable (zählt die Anzahl der Recktecke)
	// Zählvariable muss eine Klassenvariable sein, das bedeutet, es darf nur ein Exemplr für die gesamte Klasse geben
	private static int anzahlRechtecke = 0;
	
	// Der Compiler erzeugt automatisch einen Konstruktor
	// Ausnahme: der Compiler tut das nicht, wenn man selber einen Konstruktor definiert
	// Der Konstruktor ist eine Methode zur Erzeugung eines Objekts mit dem Schlüsselwort new
	// Der Konstruktor MUSS genauso heißen wie die Klassen und hat keinen Rückgabetypen (auch nicht void)
	public Rechteck() { // Standardkonstruktor soll ein Rechteck mit a = 1 und b = 2 erzeugen
		this(1, 2);
	}
	
	public Rechteck(double a) {
//		setA(a);
//		setB(a);
//		Alternativ: Aufruf eines anderen Kosntruktors in derselben Klasse (muss aber die erste Anweisung sein)
		this(a, a);
	}
	
	public Rechteck(double a, double b) {
		setA(a);
		setB(b);
		anzahlRechtecke++;
	}
	
	// getter- und setter-Methoden
	public static int getAnzahlRechtecke() {
		return anzahlRechtecke;
	}
	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		if (a < 0) {
			a = 1;
		}
//		Referenz auf das eigene Objekt
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		if (b < 0) {
			b = 1;
		}
		this.b = b;
	}

	public double getUmfang() {
		return 2 * (a + b);
	}
	
	public double getFlaeche() {
		return a * b;
	}
	
	public boolean istQuadrat() {
		return a == b;
	}
	
	public double getDiagonale() {
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}
	
	public void info() {
		if (!istQuadrat()) {
			System.out.println("Ich bin ein Rechteck mit den Seitenlängen " + a + " und " + b);
		} else {
			System.out.println("Ich bin ein Quadrat mit der Seitenlänge " + a);
		}
			
	}
}
