package oo;

public class FinalizeTest {

	public static void main(String[] args) {
		Finalizer f = null;
		for (int i = 0; i <100_000; i++ ) {
			f = new Finalizer(i);
		}
		System.gc();
//		Wie viele Objekte sind nach Zeile 9 "elligible" für die Garbage Collection? 999 Objekte (Finalizer(0) bis Finalizer (998)
		System.out.println(f.i);
//		Zugriff auf das wieder auferstandene Objekt
		System.out.println(Finalizer.f.i);
		Finalizer.f = null; // falls das Objekt gelöscht wird, wird finalize nicht mehr aufgerufen!
		System.out.println("Servus");
	}

}

class Finalizer {
	
	long i;
	static Finalizer f;
	
	Finalizer(int i) {
		this.i = i;
	}
	
//	Die finalize-Methode, die aus der Klasse Object geerbt wird, wird von der Garbage Collection aufgerufen, bevor ein Objekt gelöscht wird.
//	Achtung: die finalize-Methode wird für ein Objekt höchstens ein Mal aufgerufen!
	@Override
	public void finalize() {
		f = this;
		System.err.println(i + " wird gelöscht.");
	}
	
}
