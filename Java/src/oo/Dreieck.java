package oo;

public class Dreieck {
//	Drei Variablen (Seiten)
	private double a;
	private double b;
	private double c;

//	Drei Konstruktoren
	public Dreieck() {
		this(3, 4, 5);
	}

	public Dreieck(double a) {
		this(a, a, a);
	}

	public Dreieck(double a, double b, double c) {
		checkDreieck(a, b, c);
		double temp;
		if (a > b) {
			temp = b;
			b = a;
			a = temp;
		} 
		if (a > c) {
			temp = c;
			c = a;
			a = temp;
		}
		if (b > c) {
			temp = c;
			c = b;
			b = temp;
		}
		this.setA(a);
		this.setB(b);
		this.setC(c);
	}

//	getter und setter

	public double getA() {
		return a;
	}

//	Ausnahme, weil Dreiecks-Objekt "immutable" sein sollen, das bedeutet: nach der Erzeugung können sie nicht mehr verändert werden
	private void setA(double a) {
		checkSeite(a);
		this.a = a;
	}

	public double getB() {
		return b;
	}

	private void setB(double b) {
		checkSeite(b);
		this.b = b;
	}

	public double getC() {
		return c;
	}

	private void setC(double c) {
		checkSeite(c);
		this.c = c;
	}

	private static void checkSeite(double s) {
		if (s <= 0) {
			throw new IllegalArgumentException("Seite darf nicht negativ sein: " + s);
		}
	}
	
	private static void checkDreieck(double a, double b, double c) {
		if (a + b <= c || a + c <= b || b + c <= a) {
			throw new IllegalArgumentException("Ungültiges Dreieck: " + a + " " + b + " " + c);
		}
	}

	// info-Methode
	public void info() {
		System.out.println("Ich bin ein Dreieck mit den Seitenlängen " + a + " " + b + " " + c);
	}

//	Umfang
	public double getUmfang() {
		return a + b + c;
	}

//	Ist es gleichseitig?
	public boolean istGleichseitig() {
		return a == b && b == c;
	}

//	Ist es rechtwinklig?
	public boolean istRechtwinklig() {
		return Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2);
	}

//	Fläche (Satz des Hebron)
	public double getFlaeche() {
		double s = getUmfang() / 2;
		return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}
}








