package oo;

public class VariablenTest {

	public static void main(String[] args) {
//		Primitive Variable
		int zahl = 42;
//		Referenzvariable
		Kreis kreis = new Kreis(42);
		System.out.println("zahl vor changeInt = " + zahl);
		changeInt(zahl);
		System.out.println("zahl nach changeInt = " + zahl);
		
		System.out.println("kreis vor changeKreis1 = " + kreis.getRadius());
		changeKreis1(kreis);
		System.out.println("kreis nach changeKreis1 = " + kreis.getRadius());
		
		Kreis kreis2 = new Kreis(42);
		System.out.println("kreis vor changeKreis2 = " + kreis2.getRadius());
		changeKreis2(kreis2);
		System.out.println("kreis nach changeKreis2 = " + kreis2.getRadius());
		
	}
	 
	public static void changeInt(int x) {
		x = x + 1;
		System.out.println("zahl in changeInt = " + x);
	}
	
	public static void changeKreis1(Kreis k) {
		k.setRadius(k.getRadius() + 1);
		System.out.println("kreis in changeKreis = " + k.getRadius());
	}

	public static void changeKreis2(Kreis k) {
		k = new Kreis(42);
		k.setRadius(k.getRadius() + 1);
		System.out.println("kreis in changeKreis2 = " + k.getRadius());
	}
}
