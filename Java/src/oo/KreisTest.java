package oo;

import java.util.Scanner;

import util.MyClass;

public class KreisTest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte gib den Radius des Kreises ein");
//		int radius = sc.nextInt();
//		System.out.println("Der Durchmesser ist " + MyClass.berechneDurchmesser(radius));

//		Erzeugen eines Kreis-Objekts
		Kreis k = new Kreis();
//		Compiler-Fehler, da radius private deklariert ist
//		System.out.println(k.radius); // Zugriff auf Eigenschaften eines Objekts über den Punkt-Operator
//		k.radius = sc.nextDouble(); // Compiler-Fehler (private)
		k.setRadius(sc.nextDouble());
		System.out.println(k.getRadius());
		k.info();
//		Berechnung des Umfangs
		System.out.println("Umfang = " + k.berechneUmfang());
//		Berechnung der Fläche (radius * radius * PI)
		System.out.println("Fläche = " + k.berechneFlaeche());
		
		Kreis k1 = new Kreis(6);
		k1.info();
//		Berechnung des Umfangs
		System.out.println("Umfang = " + k1.berechneUmfang());
//		Berechnung der Fläche (radius * radius * PI)
		System.out.println("Fläche = " + k1.berechneFlaeche());
		
		Kreis k2 = new Kreis();
		k2.info();
//		Berechnung des Umfangs
		System.out.println("Umfang = " + k2.berechneUmfang());
//		Berechnung der Fläche (radius * radius * PI)
		System.out.println("Fläche = " + k.berechneFlaeche());
	}
	
}
