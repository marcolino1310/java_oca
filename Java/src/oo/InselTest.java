package oo;

public class InselTest {

	public static void main(String[] args) {
		Insel i1 = new Insel("i1");
		Insel i2 = new Insel("i2");
		Insel i3 = new Insel("i3");
		Insel i4 = new Insel("i4");
		
		i1.nachbarInsel = i2;
		i2.nachbarInsel = i3;
		i3.nachbarInsel = i4;
		i4.nachbarInsel = i1;
		
		i1 = null;
		i2 = null;
		i3 = null;
//		Wie viele Objekte sind nach Zeile 18 "elligible" für die Garbage Collection? 0
		System.out.println(i4.nachbarInsel.nachbarInsel.nachbarInsel);
		i4 = null;
//		Wie viele Objekte sind nach Zeile 21 "elligible" für die Garbage Collection? Alle vier!
		
	}

}

class Insel {
	
	String name;
	Insel nachbarInsel;
	
	Insel(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
}