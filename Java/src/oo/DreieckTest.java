package oo;

public class DreieckTest {

	public static void main(String[] args) {
		Dreieck d1 = new Dreieck();
		d1.info(); // Ich bin ein Dreieck mit den Seitenlängen 3 4 5
		Dreieck d2 = new Dreieck(4);
		d2.info(); // Ich bin ein Dreieck mit den Seitenlängen 4 4 4
		Dreieck d3 = new Dreieck(6, 10, 8);
		d3.info(); // Ich bin ein Dreieck mit den Seitenlängen 6 7 8
		System.out.println("Umfang: " + d1.getUmfang());
		System.out.println("Umfang: " + d2.getUmfang());
		System.out.println("Umfang: " + d3.getUmfang());

		System.out.println("Ist gleichseitig?: " + d1.istGleichseitig());
		System.out.println("Ist gleichseitig?: " + d2.istGleichseitig());
		System.out.println("Ist gleichseitig?: " + d3.istGleichseitig());

		System.out.println("Ist rechtwinklig?: " + d1.istRechtwinklig());
		System.out.println("Ist rechtwinklig?: " + d2.istRechtwinklig());
		System.out.println("Ist rechtwinklig?: " + d3.istRechtwinklig());

		System.out.println("Fläche: " + d1.getFlaeche());
		System.out.println("Fläche: " + d2.getFlaeche());
		System.out.println("Fläche: " + d3.getFlaeche());

		System.out.println("d1 = " + d1);
	}

}
