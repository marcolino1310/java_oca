package oo;

public class VererbungTest {

	public static void main(String[] args) {
//		Base base = new Base(42);
//		Sub sub = new Sub();
//		System.out.println(sub.i);
//		base.doIt();
//		sub.doIt();
//		Bei der Zuweisung von Referenzvariablen sind nicht nur Objekte vom selben Typ erlaubt, sondern auch Subtypen (Unterklassen)
//		Sub sub = new Base(); // Compiler-Fehler, da Base Superklasse/Oberklasse/Basisklasse von Sub
//		Polymorphie: ein Objekt in Java hat viele Typen (Gestalten) 
//		Ein Objekt vom Typ Sub ist vom Typ: Sub, Base, Object
//		Erst zur Laufzeit entscheidet der Typ des Objekts (nicht der Typ der Referenzvariablen), welche Methode ausgewählt wird
//		Späte Bindung (late binding)
		Base base1 = new Sub(); // funktioniert, da Sub ein Subtyp von Base ist
		base1.doIt();
		Base base2 = new Base();
		base2.doIt();
		Sub sub = new Sub();
		sub.doIt(42);
//		base1.doIt(42); // Compiler-Fehler: base1 ist vom Typ Base, dort gibt es keine doIt(int)-Methode
		sub = (Sub) base1; // die Variable base1 vom Type Base wird gecastet auf Sub
		sub.doIt(43);
		System.out.println(base1.i);
	}

}

class Base {
	int i;
	
	Base() {
		this(42);
	}
	
	Base(int i) {
		this.i = i;
	}
	
	public void doIt() {
		System.out.println("do it Base");
	}

}

class Sub extends Base {
	
	Sub() {
		super();
	}
	
	// Überschreiben (override)
	public void doIt() {
		System.out.println("do it Sub");
	}
	
	// Überladen (overload)
	public void doIt(int i) {
		this.i = i;
		System.out.println("do it Sub " + i);
	}
}
