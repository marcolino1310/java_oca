package oo;

// Eine Klasse ist ein Bauplan für Objekte. 
// Objekte einer Klasse haben dieselben Eigenschaften und dieselben Methoden.
public class Kreis {
//	jeder Kreis (jedes Kreis-Objekt) hat einen Radius
//	Schutz vor Zugriff von außen (Kapselung)
	private double radius;

//	Definition der Konstruktoren
//	Standard-Konstruktor (default constructor oder no-arg-constructor) hat keine Argumente
	public Kreis() { // Einheitskreis
		this(1);
	}

	public Kreis(double r) {
		setRadius(r);
	}

//	getter- und setter-Methoden
//	nach Java-Beans-Standard
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		if (radius < 0) {
			this.radius = 1;
		} else {
			this.radius = radius;
		}
	}

//	für jeden Kreis kann man den Umfang berechnen
//	Objektmethode / Instanzmethode
	public double berechneUmfang() {
		return 2 * radius * Math.PI;
	}

	public double berechneFlaeche() {
		return Math.pow(radius, 2) * Math.PI;
	}

	public double berechneDurchmesser() {
		return 2 * radius;
	}

	public void info() {
		System.out.println("Ich bin ein Kreis mit Radius " + radius);
	}

}
