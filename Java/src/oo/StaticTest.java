package oo;

public class StaticTest {
	
	private static int staticX = 42;
	private int nonStaticY = 77;

	public static void main(String[] args) {
		System.out.println(staticX); 
//		System.out.println(nonStaticY);
//		Zugriff auf eine nicht-statische Variable in einer statischen Methode geht nur über ein Objekt
		StaticTest test = new StaticTest();
		System.out.println(test.nonStaticY);
		System.out.println(new StaticTest().nonStaticY); // anonymes Objekt
		System.out.println(Testy.a);
//		System.out.println(Testy.b); // Compiler-Fehler: kein Zugriff auf Objekt-Variable über den Klassennamen
		System.out.println(new Testy().b);
		Testy testy = new Testy();
		System.out.println(testy.a); // Compiler-Warnung bei Zugriff auf statische Member über eine Referenz (anstatt der Klasse)
		System.out.println(testy.b);
	}

	public static void doItStatic() {
		System.out.println("doItStatic");
//		this.doItNonStatic(); // Compiler-Fehler: kein Zugriff auf eine nicht-statische Methode aus einer statischen Methode
//		System.out.println(staticX);
//		System.out.println(nonStaticY); // Compiler-Fehler: kein Zugriff auf eine nicht-statisches Variable aus einer statischen Methode
	}
	
	public void doItNonStatic() {
		System.out.println("doItNonStatic");
		doItStatic(); // Zugriff auf statische Member aus nicht-statischen Methoden möglich
		System.out.println(staticX); // siehe oben
		System.out.println(nonStaticY);
	}
	
}

class Testy {
	
	static int a = 1;
	int b = 2;
}
