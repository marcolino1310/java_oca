package oo;

public class BruchTest {

	public static void main(String[] args) {
//		Erzeugen Sie eine Klasse Bruch zur Verwendung von Bruchrechnung unter Beachtung der folgenden Punkte:
//			•	Zähler und Nenner sind ganzzahlig
//			•	Der Nenner darf nicht 0 sein (IllegalArgumentException)
//			•	Der Konstruktor soll den Bruch initialisieren,  der Standard-Konstruktor soll den Bruch 1/1 erzeugen
//			•	Die Klasse soll Methoden zur Realisierung der Grundrechenarten(Addition, Subtraktion, Multiplikation, Division) enthalten
//			•	Es soll eine Methode info() zur Darstellung des Bruchs auf der Konsole, z. B. 2/3 implementiert werden. Unechte Brüche sollen als gemischte Brüche dargestellt werden, z.B. 1 2/3 anstatt 5/3.
//			•	Erweitern Sie die Klasse derart, dass Brüche schon bei ihrer Erzeugung gekürzt werden, so dass Zähler und Nenner teilerfremd sind

		Bruch b1 = new Bruch(); 
		Bruch b2 = new Bruch(4); 
		Bruch b3 = new Bruch(3,4); 
		Bruch b4 = new Bruch(-2,3);
		b1.info(); // 1/1
		b2.info(); // 4/1
		b3.info(); // 3/4
		b4.info(); // 2/3
		Bruch erg = b3.multiplizieren(b4);
		erg.info(); // 6/12
		erg = b3.dividieren(b4);
		erg.info(); // 9/8
		erg = b3.addieren(b4);
		erg.info(); // 17/12
		erg = b3.subtrahieren(b4);
		erg.info(); // 1/12
		Bruch b5 = new Bruch(4, 8);
		Bruch b6 = new Bruch(10, 6);
		erg = b5.multiplizieren(b6);
		erg.info(); // 5/6
		erg = b5.dividieren(b6);
		erg.info(); // 3/10
		erg = b5.addieren(b6);
		erg.info(); // 13/6
		erg = b5.subtrahieren(b6);
		erg.info(); // -7/6
		
	}

}
