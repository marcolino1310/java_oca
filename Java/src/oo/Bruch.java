package oo;

public class Bruch {
	
//	2 Variablen
	private int zaehler;
	private int nenner;
	
//	3 Konstruktoren
	public Bruch() {
		this(1);
	}
	public Bruch (int zaehler) {
		this(zaehler, 1);
	}
	public Bruch(int zaehler, int nenner) {
		int ggT = getGGT(Math.abs(zaehler), Math.abs(nenner));
		setZaehler(zaehler/ggT);
		setNenner(nenner/ggT); 
	}
	
//	getter und setter
//	Bruch-Objekte sind immutable (unveränderbar)
	public int getZaehler() {
		return zaehler;
	} 
	private void setZaehler(int zaehler) {
		this.zaehler = zaehler;
	}
	public int getNenner() {
		return nenner;
	} 
	private void setNenner(int nenner) {
		if (nenner == 0) {
			throw new IllegalArgumentException("Der Nenner darf nicht 0 sein.");
		}
		if (nenner < 0) {
//			nenner *= -1;
//			nenner = -nenner;
			nenner = Math.abs(nenner);
			setZaehler(zaehler * -1);
		}
		this.nenner = nenner;
	}
	
//	info-Methode
	public void info() {
		System.out.print(zaehler + "/" + nenner + " = ");
		if (Math.abs(zaehler) < nenner) {
			System.out.println(zaehler + "/" + nenner);
		} else if (zaehler % nenner == 0){
			System.out.println(zaehler / nenner);
		} else {
			System.out.println(zaehler / nenner + " " + Math.abs(zaehler) % nenner + "/" + nenner );
		}
	}
	
//	Arithmetischen Operationen
	public Bruch multiplizieren(Bruch b) {
		int zaehler = this.zaehler * b.zaehler;
		int nenner = this.nenner * b.nenner;
		Bruch bruch = new Bruch(zaehler, nenner);
		return bruch;
	}

	public Bruch dividieren(Bruch b) {
		int zaehler = this.zaehler * b.nenner;
		int nenner = this.nenner * b.zaehler;
		return new Bruch(zaehler, nenner);
	}
	
	public Bruch addieren(Bruch b) {
		int zaehler = this.zaehler * b.nenner + b.zaehler * this.nenner;
		int nenner = this.nenner * b.nenner;
		return new Bruch(zaehler, nenner);
	}
	
	public Bruch subtrahieren(Bruch b) {
		int zaehler = this.zaehler * b.nenner - b.zaehler * this.nenner;
		int nenner = this.nenner * b.nenner;
		return new Bruch(zaehler, nenner);
	}
	
	private static int getGGT(int zahl1, int zahl2) {
//		int min = Math.min(zah1, zahl2);
		int min = zahl1 <= zahl2 ? zahl1 : zahl2;
		for (int i = min; i > 1; i--) {
			if (zahl1 % i == 0 && zahl2 % i == 0) {
				return i;
			}
		}
		return 1;
	}
}








