package oo.inter;

public class VererbungTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

interface InterA {
	
	void doIt();
	
}

// Ein Interface kann beliebig viele Interfaces erweitern
interface InterB extends InterA {
	
	int test();

}

interface InterC extends InterA {
	
	void test();
	
}

//	Implementierung von test() einerseits mit int und andererseits mit void ist nicht möglich
//class InterClass implements InterB, InterC {
	
//	@Override
//	public void doIt() {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void test() {
//		// TODO Auto-generated method stub
//		return ;
//	}
		
//}
