package oo.inter;

import java.util.Arrays;

public class InterTest {

	public static void main(String[] args) {
		Auto a1 = new Auto();
		a1.setPs(200);
		a1.setVerleihpreis(30);
		Auto a2 = new Auto();
		a2.setPs(150);
		a2.setVerleihpreis(20);
		Auto a3 = new Auto();
		a3.setPs(250);
		a3.setVerleihpreis(40);
		
		Sportwagen porsche = new Sportwagen();
		porsche.setPs(300);
		porsche.setVerleihpreis(99);
		
		Auto[] autos = {a1, a2, a3, porsche};
		Arrays.sort(autos);
		for(Auto auto: autos) {
			System.out.println(auto.getPs());
		}
				
		DVD dvd1 = new DVD();
		dvd1.setVerleihpreis(2.5);
		System.out.println(a1.getVerleihdauer());
		System.out.println(dvd1.getVerleihdauer());
		System.out.println(Auto.VERLEIHSTEUER);
		System.out.println(DVD.VERLEIHSTEUER);
		
		Verleihobjekt v = porsche;
		Auto a = porsche;
		Fahrzeug f = porsche;
		Comparable<Auto> c = porsche;
		Object o = porsche;
		Sportwagen s = porsche;		
		
	}

}

class Fahrzeug {
	private int ps;

	public int getPs() {
		return ps;
	}

	public void setPs(int ps) {
		this.ps = ps;
	}
}

// In Java gibt es keine Mehrfachvererbung, eine Klasse kann nur EINE Klasse erweitern (= EINE Klasse als Superklasse haben)
// Eine Klasse kann nur eine Klasse erweitern, aber beliebig viele Interfaces implementieren
// Wenn eine Klasse ein Interface implementiert, dann ist sie verpflichtet, alle abtsrakten Methoden des Interfaces zu implementieren
class Auto extends Fahrzeug implements Verleihobjekt, Comparable<Auto>{

	private double verleihpreis;

	@Override
	public double getVerleihpreis() {
		return verleihpreis;
	}

	@Override
	public void setVerleihpreis(double p) {
		verleihpreis = p;		
	}

	@Override
	public int compareTo(Auto a) {
		return getPs() - a.getPs();
	}	
	
}

class Sportwagen extends Auto {}

class DVD implements Verleihobjekt{

	private double verleihpreis;
	
	@Override
	public double getVerleihpreis() {
		return verleihpreis;
	}

	@Override
	public void setVerleihpreis(double p) {
		verleihpreis = p;		
	}

	@Override
	public int getVerleihdauer() {
		return 5;
	}
}

class Hund {}

// Ein Interface (Schnittstelle) ein abstrakter Typ. Ein Interface bestand bis Java 8 nur aus abstrakten Methoden und statischen Konstanten 
interface Verleihobjekt {
	
//	Alle Felder in einem Interface sind implizit public static final
	int VERLEIHSTEUER = 15;
	
//	Methoden in einem Interface, die nicht besonders gekennzeichnet sind, sind implizit public abstract
	double getVerleihpreis();
	void setVerleihpreis(double p);
	
//	Seit Java 8 gibt es die Möglichkeit, in Interfaces Methoden mit einem Standardverhalten zu definieren (ähnlich wie in abstrakten Klassen)
	
	default int getVerleihdauer() { // implizit public
		return 7;
	}
	
}





