package oo;

public class FinalTest {

	public static void main(String[] args) {
		final int MWST = 19;
//		MWST = 20; // Compiler-Fehler, wegen final keine neue Initialisierung (Wertzuweisung) möglich
		final Kreis k = new Kreis(5);
		k.setRadius(7);
		k.info();
//		k = new Kreis(42); // Compiler-Fehler, wegen final darf Referenzvariable nicht auf ein neues oder anderes Objekt referenzieren
		Test t = new Test();
		t = new Test(7654321, "Peter Meier");
		t.setName("Peter Schmidt");
		System.out.println(Test.MWST);
	}

}

class Test {
//	final Objektvariable muss initialisiert werden (spätestens im Konstruktor)
	private final int id;
	private String name;
//	final Klassenvariable muss initialisiert werden
	public static final int MWST = 19;
	
	Test() {
		this(123456789, "Max Mustermann");
	}
	
	Test(int id, String name) {
		this.id = id;
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
}
