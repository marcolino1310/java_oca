package oo;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck r1 = new Rechteck();
		Rechteck r2 = new Rechteck();
		Rechteck r3 = new Rechteck();
		r2 = new Rechteck(3,4);

//		Rechteck.anzahlRechtecke--;  // Compiler-Fehler wegen private
		r1.setA(4);
		r1.setB(5);
		double umfang = r1.getUmfang();
		System.out.println("Umfang: " + umfang);
		System.out.println("Fläche: " + r1.getFlaeche());
		r1.info();
		System.out.println("Ist Quadrat?: " + r1.istQuadrat()); // false
		System.out.println("Diagonale: " + r1.getDiagonale()); // 5

		umfang = r2.getUmfang();
		System.out.println("Umfang: " + umfang);
		System.out.println("Fläche: " + r2.getFlaeche());
		r2.info();
		System.out.println("Ist Quadrat?: " + r2.istQuadrat()); // false
		System.out.println("Diagonale: " + r2.getDiagonale()); // 5

		umfang = r3.getUmfang();
		System.out.println("Umfang: " + umfang);
		System.out.println("Fläche: " + r3.getFlaeche());
		r3.info();
		System.out.println("Ist Quadrat?: " + r3.istQuadrat()); // false
		System.out.println("Diagonale: " + r3.getDiagonale()); // 5
		
//		Anzahl der bisher erzeugten Rechteck-Objekte
//		Compiler-Warnung: statische Mitglieder (Variablen oder Methoden) sollten über den Klassennamen angesprochen 
//		werden und nicht über eine Referenzvariable
//		System.out.println("Anzahl der Rececke: " + r3.anzahlRechtecke);
		System.out.println("Anzahl der Rechtecke: " + Rechteck.getAnzahlRechtecke());
	}

}
