package oo;

public class ShadowingTest {
	
	String name = "Hallo";
	static int zahl = 7;

	public static void main(String[] args) {
//		Verdecken von Objekt-/Klassenvariablen mit lokalen oder Blockvariablen (Shadowing)
		System.out.println(zahl);
		int zahl = 42;
		String name = "Java";
		System.out.println(name);
		System.out.println(zahl);
		print();
	}
	
	public static void print() {
//		System.out.println(name); // Compiler-Fehler: kein Zugriff auf Objekt-Variablen innerhalb statischer Methoden
		ShadowingTest t = new ShadowingTest();
		System.out.println(t.name);
		System.out.println(zahl);
	}

}
