package oo;

public class InitTest {

	public static void main(String[] args) {
//		System.out.println(Init.zaehler);
		Init i1 = new Init();
		Init i2 = new Init();
		Init i3 = new Init();
//		test(); // Compiler-Fehler: kein statischer Zugriff auf Instanzmethoden
//		Nur mit Erzeugung eines Objekt möglich
		InitTest t = new InitTest();
		t.test();
	}
	
	public void test() {
		System.out.println("test");
	}

}

class Init {
	
//	Deklaration der statischen Felder
	public static int zaehler;
	public static final int MWST; // muss spätestens im statischen Init-Block intialisiert werden
	
//	Deklaration der Instanz-Variablen
	private int id;
	private String bezeichnung;
	private int rabattstufe;
	
//	Initialisierung der statischen Felder
//	Der statische Init-Block wird nur einmal ausgeführt (wenn die Klasse geladen wird)
	static {
		System.out.println("statischer Init-Block 1");
		zaehler = 0;
		MWST = 19;
//		rabattstufe = 1; // kein statischer Zugriff auf Instanzvariablen
	}
	
//	Initialisierung der Instanz-Variablen
//	Der Init-Block wird beim Erzeugen eines Objekts ausgeführt
	{
		System.out.println("Init-Block 1");
		rabattstufe = 1;
	}
	
	Init() {
		super(); // Immer die erste Anweisung im Konstruktor
//		Dann werden eventuell vorhandene Init-Blöcke aufgerufen.
//		Dann erst werden die weiteren Anweisungen im Konstruktor ausgeführt
		System.out.println("Konstruktor");		
	}
	
	{
		System.out.println("Init-Block 2");
		bezeichnung = "Test";
	}
	
	
	static {
		System.out.println("statischer Init-Block 2");
	}
	
}