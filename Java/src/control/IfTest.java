package control;

public class IfTest {

	public static void main(String[] args) {

		int zahl = 0;

		// Überprüfung, ob Variable positiv ist
		// Zweiseitige Auswahl
//        if (zahl > 0) {//            System.out.println("Die Zahl ist positiv.");
//            System.out.println("Die Zahl ist größer als 0.");
//        } else {
//            System.out.println("Die Zahl ist nicht positiv.");
//            System.out.println("Die Zahl ist nicht größer als 0");
//        }          

		// Dreiseitige Auswahl

		if (zahl > 0) {
			System.out.println("Die Zahl ist positiv.");
			System.out.println("Die Zahl ist größer als 0.");
		} else {
			if (zahl < 0) {
				System.out.println("Die Zahl ist negativ.");
				System.out.println("Die Zahl ist kleiner als 0.");
			} else {
				System.out.println("Die Zahl ist weder positiv noch negativ.");
				System.out.println("Die Zahl ist gleich 0.");
			}
		}

		if (zahl > 0) {
			System.out.println("Die Zahl ist positiv.");
			System.out.println("Die Zahl ist größer als 0.");
		} else if (zahl < 0) {
			System.out.println("Die Zahl ist negativ.");
			System.out.println("Die Zahl ist kleiner als 0.");
		} else if (zahl == 0) { // nur else funktioniert auch
			System.out.println("Die Zahl ist weder positiv noch negativ.");
			System.out.println("Die Zahl ist gleich 0.");
		} else {
			System.out.println("Ungültige Zahl!");
		}

	}

}
