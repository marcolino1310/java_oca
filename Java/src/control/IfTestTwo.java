// �bung zu Eingabe durch den Benutzer

package control;

import java.util.Scanner;

public class IfTestTwo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Bitte Zahl eingeben.");

		int zahl1 = sc.nextInt();

		System.out.println("Die Zahl lautet: " + zahl1);

		System.out.println("Bitte Zahl eingeben.");

		int zahl2 = sc.nextInt();

		/*
		 * �berpr�fung, ob beide Zahlen gerade sind:
		 * 
		 * Zahl geteilt durch 2, ohne Rest, ist gerade
		 * 
		 * & ist ein einfacher &-Operator
		 * 
		 * && ist ein short-circuit-Operator d.h falls der erste
		 * 
		 * Wer False ist, werden die weiteren Bedingungen nicht
		 * 
		 * mehr ausgewertet und abgebrochen. Das ist sinnvoll,
		 * 
		 * da schneller, weil alle folgenden Werte nicht mehr
		 * 
		 * ausgewertet werden
		 * 
		 */

		if (zahl1 % 2 == 0 && zahl2 % 2 == 0) {

			System.out.println("Beide Zahlen sind gerade Zahlen");

		} else {

			System.out.println("Mindestens eine Zahl ist ungerade.");

		}

		/* Überprüfung, ob mindestens eine der beiden Zahlen ungerade ist. */

		if (!(zahl1 % 2 == 0 && zahl2 % 2 == 0)) {

			System.out.println("Mindestens eine Zahl ist ungerade.");

		} else {

			System.out.println("Beide Zahlen sind gerade.");

		}

		/*
		 * Überprüfung, ob mindestens eine der beiden Zahlen gerade ist.
		 * 
		 * mittels short-circuit-ODER-Operator
		 * 
		 * Prüft auch nicht weiter, wenn der erste Ausdruck TRUE ist, also
		 * 
		 * die erste Zahl gerade.
		 */

		if (zahl1 % 2 == 0 || zahl2 % 2 == 0) {

			System.out.println("Eine Zahl ist gerade.");

		} else {

			System.out.println("Beide Zahlen sind ungerade.");

		}

		/*
		 * Überprüfung, ob genau (stop) eine der Zahlen gerade ist
		 * 
		 * mittels ^ (Spitzdach).
		 * 
		 * ^ist ein Exklusiv-ODER-Operator
		 */

		if (zahl1 % 2 == 0 ^ zahl2 % 2 == 0) {

			System.out.println("Genau eine der Zahlen ist gerade.");

		} else {

			System.out.println("Beide Zahlen sind ungerade oder gerade.");

		}

		/*
		 * Überprüfung, ob Zahl1 ungerade ist.
		 * 
		 */

		if (!(zahl1 % 2 == 0)) {

			System.out.println("Zahl 1 ist ungerade.");

			/*
			 * 
			 * mindestenst eine der beiden Zahlen ist ungerade
			 */

			if (zahl1 % 2 != 0 || zahl2 % 2 != 0) {

				System.out.println("Mindestens eine Zahl ist ungerade.");

			} else {

				System.out.println("Beide Zahlen sind gerade oder ungerade.");

			}

		}

//Eingabe-Abfrage

		/*
		 * Überprüfung, ob beide Zahlen gerade sind:
		 * 
		 * Zahl geteilt durch 2, ohne Rest, ist gerade
		 * 
		 * & ist ein einfacher &-Operator
		 * 
		 * && ist ein short-circuit-Operator d.h falls der erste
		 * 
		 * Wer Flse ist, werden die weiteren Bedingungen nicht
		 * 
		 * mehr ausgewertet und abgebrochen. Das ist sinnvoll,
		 * 
		 * da schneller, weil alle folgenden Werte nicht mehr
		 * 
		 * ausgewertet werden
		 * 
		 */

		if (zahl1 % 2 == 0 && zahl2 % 2 == 0) {

			System.out.println("Beide Zahlen sind gerade Zahlen");

		} else {

			System.out.println("Mindestens eine Zahl ist ungerade.");

		}

		/* Überprüfung, ob mindestens eine der beiden Zahlen ungerade ist. */

		if (!(zahl1 % 2 == 0 && zahl2 % 2 == 0)) {

			System.out.println("Mindestens eine Zahl ist ungerade.");

		} else {

			System.out.println("Beide Zahlen sind gerade.");

		}

		/*
		 * Überprüfung, ob mindestens eine der beiden Zahlen gerade ist.
		 * 
		 * mittels short-circuit-ODER-Operator
		 * 
		 * Prüft auch niht weiter, wenn der erste Ausdruck TRUE ist, also
		 * 
		 * die erste Zahl gerade.
		 */

		if (zahl1 % 2 == 0 || zahl2 % 2 == 0) {

			System.out.println("Eine Zahl ist gerade.");

		} else {

			System.out.println("Beide Zahlen sind ungerade.");

		}

		/*
		 * �Überprüfung, ob genau (stop) eine der Zahlen gerade ist
		 * 
		 * mittels ^ (Spitzdach).
		 * 
		 * ^ ist ein Exklusiv-ODER-Operator
		 */

		if (zahl1 % 2 == 0 ^ zahl2 % 2 == 0) {

			System.out.println("Genau eine der Zahlen ist gerade.");

		} else {

			System.out.println("Beide Zahlen sind ungerade oder gerade.");

		}

		/*
		 * Überprüfung, ob Zahl1 ungerade ist.
		 * 
		 */

		if (!(zahl1 % 2 == 0)) {

			System.out.println("Zahl 1 ist ungerade.");

		}

		if (zahl1 % 2 != 0 || zahl2 % 2 != 0) {

			System.out.println("Mindestens eine Zahl ist ungerade.");

		} else {

			System.out.println("Beide Zahlen sind gerade.");

		}

	}

}
