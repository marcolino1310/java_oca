package control;

import java.util.Scanner;
import util.MyClass;

public class StaticMethodTest {

	public static void main(String[] args) {
//		Ein Jahr ist ein Schaltjahr, wenn es durch 4 teilbar ist.
//		Ausnahme: Jahrhundertwenden sind nur ein Schaltjahr, wenn sie durch 400 teilbar sind
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Jahreszahl ein.");
		int jahr = sc.nextInt();
		System.out.println(MyClass.istSchaltjahr(jahr));
		if (MyClass.istSchaltjahr(jahr)) { // Kurzform von: if (MyClass.istSchaltjahr(x) == true)
			System.out.println("SCHALTJAHR!!!");
		}
		if (!MyClass.istSchaltjahr(jahr)) { // Kurzform von: if (MyClass.istSchaltjahr(x) == false)
			System.out.println("Gemeinjahr");
		}
		
		// Bedingungsoperator: Bedingung ? Dann-Wert : Sonst-Wert
		String s = MyClass.istSchaltjahr(jahr) ? "" : "k";
		System.out.println(jahr + " ist " + s + "ein Schaltjahr");
		System.out.println(jahr + " ist " + (MyClass.istSchaltjahr(jahr) ? "" : "k") + "ein Schaltjahr");

		
		double endkapital = MyClass.berechneEndkapital(1000, 5, 10);
		System.out.println("endkapital = " + endkapital);
		
	}
	
	

}
