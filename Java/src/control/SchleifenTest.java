package control;

import util.MyClass;

public class SchleifenTest {

	public static void main(String[] args) {
		// Summe über die ersten 100 natürlichen Zahlen
		int summe = 0;
		
		// Schleife
		for (int i = 1; i <= 100; i++) {
			summe += i;
		}
		
		System.out.println("summe = " + summe);
		
		// Fakultät 
		// 7! Möglichkeiten, sich auf die Plätze in der Klasse zu verteilen
		// 7! = 7 * 6 * 5 * 4 * 3 * 2 * 1
		final int anzahlTeilnehmer = 20; // konstant
		long fakultaet = 1;

		for (int i = 1; i <= anzahlTeilnehmer; i++) {
			fakultaet *= i;
		}
		
		System.out.printf("Anzahl der möglichen Kombinationen = %,d%n", fakultaet);
		
		System.out.printf("Anzahl der möglichen Kombinationen = %,d%n", MyClass.berechneFakultaet(anzahlTeilnehmer));

		System.out.printf("Anzahl der möglichen Kombinationen = %,d%n", MyClass.berechneFakultaetRekursiv(anzahlTeilnehmer));
		
		int zahl = 46;
		long fibo = MyClass.berechneFibonacci(zahl);
		System.out.printf("fibo = %,d%n",fibo);

	}

}
