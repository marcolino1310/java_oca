package control;

import java.util.Scanner;

public class WhileTest {

	public static void main(String[] args) {
		// Eingabe von Noten (1-6) durch den Benutzer, solange, bis er 0 eingibt.
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Noten ein: 0 für Ende.");
		
//		Kopfgesteuerte Schleife (Bedingung steht im Schleifenkopf)
//		Es ist möglich, dass die Schleife kein einziges Mal durchlaufen wird
//		while (zahl != 0) {
//			System.out.println("Achtung, Schleife!");
//			zahl = sc.nextInt();
//			System.out.println(zahl);
//		}
		
//		Fußgesteuerte Schleife (Bedingung im Schleifenfuß)
//		Es ist nicht möglich, dass die Schleife kein einziges Mal durchlaufen wird. Sie wird mindestens einmal durchlaufen.
//		int note;
//		int summe = 0;
//		int anzahl = 0;
//		do {
//			note = sc.nextInt();
//			System.out.println(note);
//			anzahl++;
////			summe = summe + note;
//			summe += note;
//		} while (note != 0);
//		anzahl--; // Anzahl korrigieren
//		System.out.println("Durchschnittsnote: " + 1.0 * summe / anzahl); // Problem: int / int ist int
//		System.out.println("Servus");
		
		int note;
		int summe = 0;
		int anzahl = 0;
		
		while (true) {
			note = sc.nextInt();
//			Sofortiger Schleifenabbruch, wenn note 0 ist!
			if (note == 0) {
				break;
			}
//			Schleifendurchgang sofort abbrechen, wenn Note größer als 6 oder kleiner als 0
			if (note > 6 || note < 0) {
				System.out.println("Ungültige Note: " + note);
				continue;
			}
			System.out.println(note);
			anzahl++;
			summe += note;
		}
		
		System.out.println("Durchschnittsnote: " + 1.0 * summe / anzahl); // Problem: int / int ist int
		System.out.println("Servus");
	}

}
