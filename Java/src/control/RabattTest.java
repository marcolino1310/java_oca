package control;

import java.text.NumberFormat;
import java.util.Scanner;

public class RabattTest {

	public static void main(String[] args) {
		// In einem Shop werden bei Bestellungen Rabatte gewährt
		// Staffelung: bei Bestellungen ab 100 Euro gibt es 2% Rabatt
		// bei Bestellungen ab 250 Euro gibt es 5% Rabatt
		// bei Bestellungen ab 500 Euro gibt es 8% Rabatt
		
		// Eingabe der Bestellsumme durch den Benutzer
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Bestellsumme in Euro ein");
		double bestellsumme = sc.nextDouble();
		
		// Bestimmung und Berechnung des Rabatts anhand der Bestellsumme
		double rabatt;
		if (bestellsumme >= 100 && bestellsumme < 250) {
			rabatt = 0.02;
		} else if (bestellsumme>= 250 && bestellsumme < 500) {
			rabatt = 0.05;
		} else if (bestellsumme >= 500) {
			rabatt = 0.08;
		} else {
			rabatt = 0;
		}
		
//		if (bestellsumme >= 500) {
//			rabatt = 0.08;
//		} else if (bestellsumme>= 250) {
//			rabatt = 0.05;
//		} else if (bestellsumme >= 100) {
//			rabatt = 0.02;
//		} else {
//			rabatt = 0;
//		}
				
		// Ausgabe des zu zahlenden Gesamtpreises
		// Testdaten: 75 Euro =>  75 Euro
		//           150 Euro => 147 Euro
		//           300 Euro => 285 Euro
		//          1000 Euro => 920 Euro
		double gesamtpreis = bestellsumme - bestellsumme * rabatt;
		System.out.printf("Gesamtpreis: %,.2f €%n", gesamtpreis);
		System.out.println("Gesamtpreis: " + NumberFormat.getCurrencyInstance().format(gesamtpreis));

		sc.close();

	}

}
