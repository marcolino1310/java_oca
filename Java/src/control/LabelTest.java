package control;

public class LabelTest {

	public static void main(String[] args) {
		// Ausgabe des großen Einmaleins
		// 11 * 11 = 121
		// 11 * 12 = 132
		// ...
		// 20 * 20 = 400

		// Doppelschleife (verschachtelte Schleife)
//		for (int i = 11; i <= 20; i++) {
//			for (int j = 11; j <= 20; j++) {
//				System.out.println(i + " * " + j + " = " + i * j);
//			}
//		}

		// Ab welchen Faktoren wird das Produkt erstmalig größer als 300?
		// break bricht immer nur die Schleife ab, in der es sich befindet.
		// Lösung: Schleife mit Label markieren
		outer: for (int i = 11; i <= 20; i++) {
			for (int j = 11; j <= 20; j++) {
				if (i * j > 300) {
					System.out.println(i + " * " + j + " = " + i * j);
//					break;
					break outer; // bricht die Schleife, auf die dieses Label zeigt, ab
				}
			}
		}

		System.out.println("Fertig");

		int ziffer1 = 6;
		int ziffer2 = 4;
		int ziffer3 = 8;
		
		outer: for (int i = 0; i <= 9; i++) {
			middle: for (int j = 0; j <= 9; j++) {
				inner: for (int k = 0; k<= 9; k++) {
					System.out.println(i + " " + j + " " + k);
					if (i == ziffer1 && j == ziffer2 && k == ziffer3) {
						System.out.println("Yippieh! Das Schloss ist offen!");
						break outer;
					}
				}
			}
		}
		
		System.out.println("Los geht's!");
	}

}






