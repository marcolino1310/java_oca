package control;

public class ForTest {

	public static void main(String[] args) throws InterruptedException {
		// Zählschleife
		// for (Deklaration; Bedingung; Inkrementierung)
		// i (Zählvariable) ist eine Blockvariable => Gültigkeit nur innerhalb der
		// Scheife (Block)
		// Ausgabe der ersten 100 natürlichen Zahlen
		for (int i = 1; i <= 100; i++) {
			System.out.println(i);
		}

//		System.out.println("Wert von i nach der Schleife: " + i); // Compiler-Fehler: i ist nur innerhlab der Schleife gültig

		System.out.println("Countdown läuft!");
		// Countdown
		for (int i = 10; i >= 0; i--) {
			System.out.println(i);
//			Thread.sleep(1000);
		}
		System.out.println("Die ersten 100 geraden Zahlen");
		int j;
		for (j = 1; j <= 200; j++) {
			if (j % 2 == 0) {
				System.out.println(j);
			}
		}
		System.out.println("Wert von j nach der Schleife: " + j);
		System.out.println("Die ersten 100 geraden Zahlen");

		for (int i = 1; i <= 100; i++) {
			System.out.println(2 * i);
		}
		
		System.out.println("Die ersten 100 geraden Zahlen");

		for (int i = 2; i <= 200; i = i + 2) {
			System.out.println(i);
		}
		
		System.out.println("Die ersten 100 durch 4 teilbaren Zahlen");

		for (int i = 4; i <= 400; i += 4) {
			System.out.println(i);
		}
		
		System.out.println("Die ersten 100 durch 4 teilbaren Zahlen");

		for (int i = 1; i <= 100; i++) {
			System.out.println(4 * i);
		}
		
		System.out.println("Die ersten 100 durch 4 teilbaren Zahlen");

		for (j = 1; j <= 400; j++) {
			if (j % 4 == 0) {
				System.out.println(j);
			}
		}
		
		System.out.println("Die ersten 100 Zahlen in Zehnerblöcken");

		for (int i = 1; i <= 100; i++) {
//			System.out.print(i + " ");
			System.out.printf("%4d", i); // ganze Zahl mit Breite 4 Zeichen, rechtsbündig
			// Zeilenumbruch, wenn i durch 10 teilbar ist
			if (i % 10 == 0) {
				System.out.println();
			}
		}
		
	}

}
